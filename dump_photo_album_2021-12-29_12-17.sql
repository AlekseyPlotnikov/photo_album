14--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5
-- Dumped by pg_dump version 13.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO "user";

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO "user";

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO "user";

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO "user";

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO "user";

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO "user";

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO "user";

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO "user";

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO "user";

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO "user";

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO "user";

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO "user";

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO "user";

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO "user";

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO "user";

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO "user";

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: fcm_django_fcmdevice; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.fcm_django_fcmdevice (
    id integer NOT NULL,
    name character varying(255),
    active boolean NOT NULL,
    date_created timestamp with time zone,
    device_id character varying(150),
    registration_id text NOT NULL,
    type character varying(10) NOT NULL,
    user_id integer
);


ALTER TABLE public.fcm_django_fcmdevice OWNER TO "user";

--
-- Name: fcm_django_fcmdevice_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.fcm_django_fcmdevice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fcm_django_fcmdevice_id_seq OWNER TO "user";

--
-- Name: fcm_django_fcmdevice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.fcm_django_fcmdevice_id_seq OWNED BY public.fcm_django_fcmdevice.id;


--
-- Name: garpix_auth_accesstoken; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_auth_accesstoken (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.garpix_auth_accesstoken OWNER TO "user";

--
-- Name: garpix_auth_refreshtoken; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_auth_refreshtoken (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.garpix_auth_refreshtoken OWNER TO "user";

--
-- Name: garpix_menu_menuitem; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_menu_menuitem (
    id integer NOT NULL,
    url character varying(1000),
    title_for_admin character varying(100) NOT NULL,
    title character varying(100) NOT NULL,
    title_ru character varying(100),
    icon character varying(100),
    menu_type character varying(100) NOT NULL,
    is_active boolean NOT NULL,
    target_blank boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    sort integer NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    page_id integer,
    parent_id integer,
    CONSTRAINT garpix_menu_menuitem_level_check CHECK ((level >= 0)),
    CONSTRAINT garpix_menu_menuitem_lft_check CHECK ((lft >= 0)),
    CONSTRAINT garpix_menu_menuitem_rght_check CHECK ((rght >= 0)),
    CONSTRAINT garpix_menu_menuitem_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.garpix_menu_menuitem OWNER TO "user";

--
-- Name: garpix_menu_menuitem_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_menu_menuitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_menu_menuitem_id_seq OWNER TO "user";

--
-- Name: garpix_menu_menuitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_menu_menuitem_id_seq OWNED BY public.garpix_menu_menuitem.id;


--
-- Name: garpix_notify_notify; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notify (
    id integer NOT NULL,
    phone character varying(30) NOT NULL,
    telegram_chat_id character varying(200) NOT NULL,
    telegram_secret character varying(150) NOT NULL,
    viber_chat_id character varying(200) NOT NULL,
    viber_secret_key character varying(200) NOT NULL,
    subject character varying(255) NOT NULL,
    text text NOT NULL,
    html text NOT NULL,
    email character varying(255),
    sender_email character varying(255),
    type integer NOT NULL,
    state integer NOT NULL,
    event integer,
    is_read boolean NOT NULL,
    data_json text,
    created_at timestamp with time zone NOT NULL,
    send_at timestamp with time zone,
    sent_at timestamp with time zone,
    category_id integer NOT NULL,
    user_id integer
);


ALTER TABLE public.garpix_notify_notify OWNER TO "user";

--
-- Name: garpix_notify_notify_files; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notify_files (
    id integer NOT NULL,
    notify_id integer NOT NULL,
    notifyfile_id integer NOT NULL
);


ALTER TABLE public.garpix_notify_notify_files OWNER TO "user";

--
-- Name: garpix_notify_notify_files_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notify_files_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notify_files_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notify_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notify_files_id_seq OWNED BY public.garpix_notify_notify_files.id;


--
-- Name: garpix_notify_notify_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notify_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notify_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notify_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notify_id_seq OWNED BY public.garpix_notify_notify.id;


--
-- Name: garpix_notify_notifycategory; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifycategory (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    template text NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.garpix_notify_notifycategory OWNER TO "user";

--
-- Name: garpix_notify_notifycategory_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifycategory_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifycategory_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifycategory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifycategory_id_seq OWNED BY public.garpix_notify_notifycategory.id;


--
-- Name: garpix_notify_notifyconfig; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifyconfig (
    id integer NOT NULL,
    periodic integer NOT NULL,
    email_max_day_limit integer NOT NULL,
    email_max_hour_limit integer NOT NULL,
    sms_url character varying(255) NOT NULL,
    sms_api_id character varying(255) NOT NULL,
    sms_from character varying(255) NOT NULL,
    telegram_api_key character varying(255) NOT NULL,
    telegram_bot_name character varying(255) NOT NULL,
    telegram_welcome_text text NOT NULL,
    telegram_help_text text NOT NULL,
    telegram_bad_command_text text NOT NULL,
    telegram_success_added_text text NOT NULL,
    telegram_failed_added_text text NOT NULL,
    viber_api_key character varying(255) NOT NULL,
    viber_bot_name character varying(255) NOT NULL,
    is_email_enabled boolean NOT NULL,
    is_sms_enabled boolean NOT NULL,
    is_push_enabled boolean NOT NULL,
    is_telegram_enabled boolean NOT NULL,
    is_viber_enabled boolean NOT NULL,
    viber_success_added_text text NOT NULL,
    viber_failed_added_text text NOT NULL,
    viber_text_for_new_sub text NOT NULL,
    viber_welcome_text text NOT NULL
);


ALTER TABLE public.garpix_notify_notifyconfig OWNER TO "user";

--
-- Name: garpix_notify_notifyconfig_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifyconfig_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifyconfig_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifyconfig_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifyconfig_id_seq OWNED BY public.garpix_notify_notifyconfig.id;


--
-- Name: garpix_notify_notifyerrorlog; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifyerrorlog (
    id integer NOT NULL,
    error text,
    created_at timestamp with time zone NOT NULL,
    notify_id integer NOT NULL
);


ALTER TABLE public.garpix_notify_notifyerrorlog OWNER TO "user";

--
-- Name: garpix_notify_notifyerrorlog_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifyerrorlog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifyerrorlog_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifyerrorlog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifyerrorlog_id_seq OWNED BY public.garpix_notify_notifyerrorlog.id;


--
-- Name: garpix_notify_notifyfile; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifyfile (
    id integer NOT NULL,
    file character varying(1000) NOT NULL,
    created_at timestamp with time zone NOT NULL
);


ALTER TABLE public.garpix_notify_notifyfile OWNER TO "user";

--
-- Name: garpix_notify_notifyfile_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifyfile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifyfile_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifyfile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifyfile_id_seq OWNED BY public.garpix_notify_notifyfile.id;


--
-- Name: garpix_notify_notifytemplate; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifytemplate (
    id integer NOT NULL,
    phone character varying(30) NOT NULL,
    telegram_chat_id character varying(200) NOT NULL,
    telegram_secret character varying(150) NOT NULL,
    viber_chat_id character varying(200) NOT NULL,
    viber_secret_key character varying(200) NOT NULL,
    title character varying(255) NOT NULL,
    subject character varying(255) NOT NULL,
    text text NOT NULL,
    html text NOT NULL,
    email character varying(255),
    type integer NOT NULL,
    event integer,
    created_at timestamp with time zone NOT NULL,
    is_active boolean NOT NULL,
    send_at timestamp with time zone,
    category_id integer NOT NULL,
    user_id integer
);


ALTER TABLE public.garpix_notify_notifytemplate OWNER TO "user";

--
-- Name: garpix_notify_notifytemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifytemplate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifytemplate_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifytemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifytemplate_id_seq OWNED BY public.garpix_notify_notifytemplate.id;


--
-- Name: garpix_notify_notifytemplate_user_lists; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifytemplate_user_lists (
    id integer NOT NULL,
    notifytemplate_id integer NOT NULL,
    notifyuserlist_id integer NOT NULL
);


ALTER TABLE public.garpix_notify_notifytemplate_user_lists OWNER TO "user";

--
-- Name: garpix_notify_notifytemplate_user_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifytemplate_user_lists_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifytemplate_user_lists_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifytemplate_user_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifytemplate_user_lists_id_seq OWNED BY public.garpix_notify_notifytemplate_user_lists.id;


--
-- Name: garpix_notify_notifyuserlist; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifyuserlist (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    mail_to_all boolean NOT NULL
);


ALTER TABLE public.garpix_notify_notifyuserlist OWNER TO "user";

--
-- Name: garpix_notify_notifyuserlist_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifyuserlist_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifyuserlist_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifyuserlist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifyuserlist_id_seq OWNED BY public.garpix_notify_notifyuserlist.id;


--
-- Name: garpix_notify_notifyuserlist_user_groups; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifyuserlist_user_groups (
    id integer NOT NULL,
    notifyuserlist_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.garpix_notify_notifyuserlist_user_groups OWNER TO "user";

--
-- Name: garpix_notify_notifyuserlist_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifyuserlist_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifyuserlist_user_groups_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifyuserlist_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifyuserlist_user_groups_id_seq OWNED BY public.garpix_notify_notifyuserlist_user_groups.id;


--
-- Name: garpix_notify_notifyuserlistparticipant; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_notifyuserlistparticipant (
    id integer NOT NULL,
    phone character varying(30) NOT NULL,
    telegram_chat_id character varying(200) NOT NULL,
    telegram_secret character varying(150) NOT NULL,
    viber_chat_id character varying(200) NOT NULL,
    viber_secret_key character varying(200) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    email character varying(255),
    user_id integer,
    user_list_id integer NOT NULL
);


ALTER TABLE public.garpix_notify_notifyuserlistparticipant OWNER TO "user";

--
-- Name: garpix_notify_notifyuserlistparticipant_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_notifyuserlistparticipant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_notifyuserlistparticipant_id_seq OWNER TO "user";

--
-- Name: garpix_notify_notifyuserlistparticipant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_notifyuserlistparticipant_id_seq OWNED BY public.garpix_notify_notifyuserlistparticipant.id;


--
-- Name: garpix_notify_smtpaccount; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_notify_smtpaccount (
    id integer NOT NULL,
    host character varying(255) NOT NULL,
    port integer NOT NULL,
    is_use_tls boolean NOT NULL,
    is_use_ssl boolean NOT NULL,
    sender character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    timeout integer NOT NULL,
    is_active boolean NOT NULL,
    email_hour_used_times integer NOT NULL,
    email_day_used_times integer NOT NULL,
    email_hour_used_date timestamp with time zone NOT NULL,
    email_day_used_date timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.garpix_notify_smtpaccount OWNER TO "user";

--
-- Name: garpix_notify_smtpaccount_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_notify_smtpaccount_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_notify_smtpaccount_id_seq OWNER TO "user";

--
-- Name: garpix_notify_smtpaccount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_notify_smtpaccount_id_seq OWNED BY public.garpix_notify_smtpaccount.id;


--
-- Name: garpix_page_basepage; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_page_basepage (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    title_ru character varying(255),
    is_active boolean NOT NULL,
    display_on_sitemap boolean NOT NULL,
    slug character varying(150) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    seo_title character varying(250) NOT NULL,
    seo_title_ru character varying(250),
    seo_keywords character varying(250) NOT NULL,
    seo_keywords_ru character varying(250),
    seo_description text NOT NULL,
    seo_description_ru text,
    seo_author character varying(250) NOT NULL,
    seo_author_ru character varying(250),
    seo_og_type character varying(250) NOT NULL,
    seo_image character varying(100),
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    parent_id integer,
    polymorphic_ctype_id integer,
    CONSTRAINT garpix_page_basepage_level_check CHECK ((level >= 0)),
    CONSTRAINT garpix_page_basepage_lft_check CHECK ((lft >= 0)),
    CONSTRAINT garpix_page_basepage_rght_check CHECK ((rght >= 0)),
    CONSTRAINT garpix_page_basepage_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.garpix_page_basepage OWNER TO "user";

--
-- Name: garpix_page_basepage_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_page_basepage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_page_basepage_id_seq OWNER TO "user";

--
-- Name: garpix_page_basepage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_page_basepage_id_seq OWNED BY public.garpix_page_basepage.id;


--
-- Name: garpix_page_basepage_sites; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpix_page_basepage_sites (
    id integer NOT NULL,
    basepage_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.garpix_page_basepage_sites OWNER TO "user";

--
-- Name: garpix_page_basepage_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.garpix_page_basepage_sites_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.garpix_page_basepage_sites_id_seq OWNER TO "user";

--
-- Name: garpix_page_basepage_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.garpix_page_basepage_sites_id_seq OWNED BY public.garpix_page_basepage_sites.id;


--
-- Name: garpixcms_page; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.garpixcms_page (
    basepage_ptr_id integer NOT NULL,
    content text NOT NULL,
    content_ru text
);


ALTER TABLE public.garpixcms_page OWNER TO "user";

--
-- Name: oauth2_provider_accesstoken; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.oauth2_provider_accesstoken (
    id bigint NOT NULL,
    token character varying(255) NOT NULL,
    expires timestamp with time zone NOT NULL,
    scope text NOT NULL,
    application_id bigint,
    user_id integer,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    source_refresh_token_id bigint,
    id_token_id bigint
);


ALTER TABLE public.oauth2_provider_accesstoken OWNER TO "user";

--
-- Name: oauth2_provider_accesstoken_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.oauth2_provider_accesstoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_accesstoken_id_seq OWNER TO "user";

--
-- Name: oauth2_provider_accesstoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.oauth2_provider_accesstoken_id_seq OWNED BY public.oauth2_provider_accesstoken.id;


--
-- Name: oauth2_provider_application; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.oauth2_provider_application (
    id bigint NOT NULL,
    client_id character varying(100) NOT NULL,
    redirect_uris text NOT NULL,
    client_type character varying(32) NOT NULL,
    authorization_grant_type character varying(32) NOT NULL,
    client_secret character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    user_id integer,
    skip_authorization boolean NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    algorithm character varying(5) NOT NULL
);


ALTER TABLE public.oauth2_provider_application OWNER TO "user";

--
-- Name: oauth2_provider_application_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.oauth2_provider_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_application_id_seq OWNER TO "user";

--
-- Name: oauth2_provider_application_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.oauth2_provider_application_id_seq OWNED BY public.oauth2_provider_application.id;


--
-- Name: oauth2_provider_grant; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.oauth2_provider_grant (
    id bigint NOT NULL,
    code character varying(255) NOT NULL,
    expires timestamp with time zone NOT NULL,
    redirect_uri text NOT NULL,
    scope text NOT NULL,
    application_id bigint NOT NULL,
    user_id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    code_challenge character varying(128) NOT NULL,
    code_challenge_method character varying(10) NOT NULL,
    nonce character varying(255) NOT NULL,
    claims text NOT NULL
);


ALTER TABLE public.oauth2_provider_grant OWNER TO "user";

--
-- Name: oauth2_provider_grant_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.oauth2_provider_grant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_grant_id_seq OWNER TO "user";

--
-- Name: oauth2_provider_grant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.oauth2_provider_grant_id_seq OWNED BY public.oauth2_provider_grant.id;


--
-- Name: oauth2_provider_idtoken; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.oauth2_provider_idtoken (
    id bigint NOT NULL,
    jti uuid NOT NULL,
    expires timestamp with time zone NOT NULL,
    scope text NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    application_id bigint,
    user_id integer
);


ALTER TABLE public.oauth2_provider_idtoken OWNER TO "user";

--
-- Name: oauth2_provider_idtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.oauth2_provider_idtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_idtoken_id_seq OWNER TO "user";

--
-- Name: oauth2_provider_idtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.oauth2_provider_idtoken_id_seq OWNED BY public.oauth2_provider_idtoken.id;


--
-- Name: oauth2_provider_refreshtoken; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.oauth2_provider_refreshtoken (
    id bigint NOT NULL,
    token character varying(255) NOT NULL,
    access_token_id bigint,
    application_id bigint NOT NULL,
    user_id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    revoked timestamp with time zone
);


ALTER TABLE public.oauth2_provider_refreshtoken OWNER TO "user";

--
-- Name: oauth2_provider_refreshtoken_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.oauth2_provider_refreshtoken_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oauth2_provider_refreshtoken_id_seq OWNER TO "user";

--
-- Name: oauth2_provider_refreshtoken_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.oauth2_provider_refreshtoken_id_seq OWNED BY public.oauth2_provider_refreshtoken.id;


--
-- Name: photo_album_photo; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.photo_album_photo (
    id integer NOT NULL,
    title_photo character varying(100),
    description_photo text,
    photo character varying(100) NOT NULL,
    thumbnail_photo character varying(100) NOT NULL,
    created_photo timestamp with time zone NOT NULL,
    album_id integer NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE public.photo_album_photo OWNER TO "user";

--
-- Name: photo_album_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.photo_album_photo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.photo_album_photo_id_seq OWNER TO "user";

--
-- Name: photo_album_photo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.photo_album_photo_id_seq OWNED BY public.photo_album_photo.id;


--
-- Name: photo_album_photo_tagPhoto; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public."photo_album_photo_tagPhoto" (
    id integer NOT NULL,
    photo_id integer NOT NULL,
    tagphoto_id integer NOT NULL
);


ALTER TABLE public."photo_album_photo_tagPhoto" OWNER TO "user";

--
-- Name: photo_album_photo_tagPhoto_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public."photo_album_photo_tagPhoto_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."photo_album_photo_tagPhoto_id_seq" OWNER TO "user";

--
-- Name: photo_album_photo_tagPhoto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public."photo_album_photo_tagPhoto_id_seq" OWNED BY public."photo_album_photo_tagPhoto".id;


--
-- Name: photo_album_photoalbum; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.photo_album_photoalbum (
    id integer NOT NULL,
    title_album character varying(100),
    created_album timestamp with time zone NOT NULL,
    author_id integer NOT NULL
);


ALTER TABLE public.photo_album_photoalbum OWNER TO "user";

--
-- Name: photo_album_photoalbum_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.photo_album_photoalbum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.photo_album_photoalbum_id_seq OWNER TO "user";

--
-- Name: photo_album_photoalbum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.photo_album_photoalbum_id_seq OWNED BY public.photo_album_photoalbum.id;


--
-- Name: photo_album_photoalbum_tagAlbum; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public."photo_album_photoalbum_tagAlbum" (
    id integer NOT NULL,
    photoalbum_id integer NOT NULL,
    tagalbum_id integer NOT NULL
);


ALTER TABLE public."photo_album_photoalbum_tagAlbum" OWNER TO "user";

--
-- Name: photo_album_photoalbum_tagAlbum_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public."photo_album_photoalbum_tagAlbum_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."photo_album_photoalbum_tagAlbum_id_seq" OWNER TO "user";

--
-- Name: photo_album_photoalbum_tagAlbum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public."photo_album_photoalbum_tagAlbum_id_seq" OWNED BY public."photo_album_photoalbum_tagAlbum".id;


--
-- Name: photo_album_tagalbum; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.photo_album_tagalbum (
    id integer NOT NULL,
    title character varying(100) NOT NULL
);


ALTER TABLE public.photo_album_tagalbum OWNER TO "user";

--
-- Name: photo_album_tagalbum_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.photo_album_tagalbum_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.photo_album_tagalbum_id_seq OWNER TO "user";

--
-- Name: photo_album_tagalbum_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.photo_album_tagalbum_id_seq OWNED BY public.photo_album_tagalbum.id;


--
-- Name: photo_album_tagphoto; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.photo_album_tagphoto (
    id integer NOT NULL,
    title character varying(100) NOT NULL
);


ALTER TABLE public.photo_album_tagphoto OWNER TO "user";

--
-- Name: photo_album_tagphoto_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.photo_album_tagphoto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.photo_album_tagphoto_id_seq OWNER TO "user";

--
-- Name: photo_album_tagphoto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.photo_album_tagphoto_id_seq OWNED BY public.photo_album_tagphoto.id;


--
-- Name: social_auth_association; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.social_auth_association (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    handle character varying(255) NOT NULL,
    secret character varying(255) NOT NULL,
    issued integer NOT NULL,
    lifetime integer NOT NULL,
    assoc_type character varying(64) NOT NULL
);


ALTER TABLE public.social_auth_association OWNER TO "user";

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.social_auth_association_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_association_id_seq OWNER TO "user";

--
-- Name: social_auth_association_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.social_auth_association_id_seq OWNED BY public.social_auth_association.id;


--
-- Name: social_auth_code; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.social_auth_code (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    code character varying(32) NOT NULL,
    verified boolean NOT NULL,
    "timestamp" timestamp with time zone NOT NULL
);


ALTER TABLE public.social_auth_code OWNER TO "user";

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.social_auth_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_code_id_seq OWNER TO "user";

--
-- Name: social_auth_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.social_auth_code_id_seq OWNED BY public.social_auth_code.id;


--
-- Name: social_auth_nonce; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.social_auth_nonce (
    id integer NOT NULL,
    server_url character varying(255) NOT NULL,
    "timestamp" integer NOT NULL,
    salt character varying(65) NOT NULL
);


ALTER TABLE public.social_auth_nonce OWNER TO "user";

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.social_auth_nonce_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_nonce_id_seq OWNER TO "user";

--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.social_auth_nonce_id_seq OWNED BY public.social_auth_nonce.id;


--
-- Name: social_auth_partial; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.social_auth_partial (
    id integer NOT NULL,
    token character varying(32) NOT NULL,
    next_step smallint NOT NULL,
    backend character varying(32) NOT NULL,
    data text NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    CONSTRAINT social_auth_partial_next_step_check CHECK ((next_step >= 0))
);


ALTER TABLE public.social_auth_partial OWNER TO "user";

--
-- Name: social_auth_partial_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.social_auth_partial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_partial_id_seq OWNER TO "user";

--
-- Name: social_auth_partial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.social_auth_partial_id_seq OWNED BY public.social_auth_partial.id;


--
-- Name: social_auth_usersocialauth; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.social_auth_usersocialauth (
    id integer NOT NULL,
    provider character varying(32) NOT NULL,
    uid character varying(255) NOT NULL,
    extra_data text NOT NULL,
    user_id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    modified timestamp with time zone NOT NULL
);


ALTER TABLE public.social_auth_usersocialauth OWNER TO "user";

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.social_auth_usersocialauth_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.social_auth_usersocialauth_id_seq OWNER TO "user";

--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.social_auth_usersocialauth_id_seq OWNED BY public.social_auth_usersocialauth.id;


--
-- Name: user_user; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.user_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    phone character varying(30) NOT NULL,
    telegram_chat_id character varying(200) NOT NULL,
    telegram_secret character varying(150) NOT NULL,
    viber_chat_id character varying(200) NOT NULL,
    viber_secret_key character varying(200) NOT NULL
);


ALTER TABLE public.user_user OWNER TO "user";

--
-- Name: user_user_groups; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.user_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.user_user_groups OWNER TO "user";

--
-- Name: user_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.user_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_user_groups_id_seq OWNER TO "user";

--
-- Name: user_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.user_user_groups_id_seq OWNED BY public.user_user_groups.id;


--
-- Name: user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.user_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_user_id_seq OWNER TO "user";

--
-- Name: user_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.user_user_id_seq OWNED BY public.user_user.id;


--
-- Name: user_user_user_permissions; Type: TABLE; Schema: public; Owner: user
--

CREATE TABLE public.user_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.user_user_user_permissions OWNER TO "user";

--
-- Name: user_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: user
--

CREATE SEQUENCE public.user_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_user_user_permissions_id_seq OWNER TO "user";

--
-- Name: user_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: user
--

ALTER SEQUENCE public.user_user_user_permissions_id_seq OWNED BY public.user_user_user_permissions.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: fcm_django_fcmdevice id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.fcm_django_fcmdevice ALTER COLUMN id SET DEFAULT nextval('public.fcm_django_fcmdevice_id_seq'::regclass);


--
-- Name: garpix_menu_menuitem id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_menu_menuitem ALTER COLUMN id SET DEFAULT nextval('public.garpix_menu_menuitem_id_seq'::regclass);


--
-- Name: garpix_notify_notify id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notify_id_seq'::regclass);


--
-- Name: garpix_notify_notify_files id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify_files ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notify_files_id_seq'::regclass);


--
-- Name: garpix_notify_notifycategory id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifycategory ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifycategory_id_seq'::regclass);


--
-- Name: garpix_notify_notifyconfig id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyconfig ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifyconfig_id_seq'::regclass);


--
-- Name: garpix_notify_notifyerrorlog id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyerrorlog ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifyerrorlog_id_seq'::regclass);


--
-- Name: garpix_notify_notifyfile id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyfile ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifyfile_id_seq'::regclass);


--
-- Name: garpix_notify_notifytemplate id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifytemplate_id_seq'::regclass);


--
-- Name: garpix_notify_notifytemplate_user_lists id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate_user_lists ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifytemplate_user_lists_id_seq'::regclass);


--
-- Name: garpix_notify_notifyuserlist id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlist ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifyuserlist_id_seq'::regclass);


--
-- Name: garpix_notify_notifyuserlist_user_groups id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlist_user_groups ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifyuserlist_user_groups_id_seq'::regclass);


--
-- Name: garpix_notify_notifyuserlistparticipant id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlistparticipant ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_notifyuserlistparticipant_id_seq'::regclass);


--
-- Name: garpix_notify_smtpaccount id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_smtpaccount ALTER COLUMN id SET DEFAULT nextval('public.garpix_notify_smtpaccount_id_seq'::regclass);


--
-- Name: garpix_page_basepage id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage ALTER COLUMN id SET DEFAULT nextval('public.garpix_page_basepage_id_seq'::regclass);


--
-- Name: garpix_page_basepage_sites id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage_sites ALTER COLUMN id SET DEFAULT nextval('public.garpix_page_basepage_sites_id_seq'::regclass);


--
-- Name: oauth2_provider_accesstoken id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_accesstoken_id_seq'::regclass);


--
-- Name: oauth2_provider_application id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_application ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_application_id_seq'::regclass);


--
-- Name: oauth2_provider_grant id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_grant ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_grant_id_seq'::regclass);


--
-- Name: oauth2_provider_idtoken id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_idtoken ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_idtoken_id_seq'::regclass);


--
-- Name: oauth2_provider_refreshtoken id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken ALTER COLUMN id SET DEFAULT nextval('public.oauth2_provider_refreshtoken_id_seq'::regclass);


--
-- Name: photo_album_photo id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_photo ALTER COLUMN id SET DEFAULT nextval('public.photo_album_photo_id_seq'::regclass);


--
-- Name: photo_album_photo_tagPhoto id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photo_tagPhoto" ALTER COLUMN id SET DEFAULT nextval('public."photo_album_photo_tagPhoto_id_seq"'::regclass);


--
-- Name: photo_album_photoalbum id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_photoalbum ALTER COLUMN id SET DEFAULT nextval('public.photo_album_photoalbum_id_seq'::regclass);


--
-- Name: photo_album_photoalbum_tagAlbum id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photoalbum_tagAlbum" ALTER COLUMN id SET DEFAULT nextval('public."photo_album_photoalbum_tagAlbum_id_seq"'::regclass);


--
-- Name: photo_album_tagalbum id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_tagalbum ALTER COLUMN id SET DEFAULT nextval('public.photo_album_tagalbum_id_seq'::regclass);


--
-- Name: photo_album_tagphoto id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_tagphoto ALTER COLUMN id SET DEFAULT nextval('public.photo_album_tagphoto_id_seq'::regclass);


--
-- Name: social_auth_association id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_association ALTER COLUMN id SET DEFAULT nextval('public.social_auth_association_id_seq'::regclass);


--
-- Name: social_auth_code id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_code ALTER COLUMN id SET DEFAULT nextval('public.social_auth_code_id_seq'::regclass);


--
-- Name: social_auth_nonce id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_nonce ALTER COLUMN id SET DEFAULT nextval('public.social_auth_nonce_id_seq'::regclass);


--
-- Name: social_auth_partial id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_partial ALTER COLUMN id SET DEFAULT nextval('public.social_auth_partial_id_seq'::regclass);


--
-- Name: social_auth_usersocialauth id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_usersocialauth ALTER COLUMN id SET DEFAULT nextval('public.social_auth_usersocialauth_id_seq'::regclass);


--
-- Name: user_user id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user ALTER COLUMN id SET DEFAULT nextval('public.user_user_id_seq'::regclass);


--
-- Name: user_user_groups id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_groups ALTER COLUMN id SET DEFAULT nextval('public.user_user_groups_id_seq'::regclass);


--
-- Name: user_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.user_user_user_permissions_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
41	Can add access token	11	add_accesstoken
42	Can change access token	11	change_accesstoken
43	Can delete access token	11	delete_accesstoken
44	Can view access token	11	view_accesstoken
45	Can add grant	12	add_grant
46	Can change grant	12	change_grant
47	Can delete grant	12	delete_grant
48	Can view grant	12	view_grant
49	Can add refresh token	13	add_refreshtoken
50	Can change refresh token	13	change_refreshtoken
51	Can delete refresh token	13	delete_refreshtoken
52	Can view refresh token	13	view_refreshtoken
53	Can add id token	14	add_idtoken
54	Can change id token	14	change_idtoken
55	Can delete id token	14	delete_idtoken
56	Can view id token	14	view_idtoken
57	Can add association	15	add_association
58	Can change association	15	change_association
59	Can delete association	15	delete_association
60	Can view association	15	view_association
61	Can add code	16	add_code
62	Can change code	16	change_code
63	Can delete code	16	delete_code
64	Can view code	16	view_code
65	Can add nonce	17	add_nonce
89	Can add Категория	23	add_notifycategory
90	Can change Категория	23	change_notifycategory
91	Can delete Категория	23	delete_notifycategory
92	Can view Категория	23	view_notifycategory
93	Can add Настройка	24	add_notifyconfig
94	Can change Настройка	24	change_notifyconfig
95	Can delete Настройка	24	delete_notifyconfig
96	Can view Настройка	24	view_notifyconfig
97	Can add Ошибка отправки уведомления	25	add_notifyerrorlog
98	Can change Ошибка отправки уведомления	25	change_notifyerrorlog
99	Can delete Ошибка отправки уведомления	25	delete_notifyerrorlog
100	Can view Ошибка отправки уведомления	25	view_notifyerrorlog
101	Can add Файл	26	add_notifyfile
102	Can change Файл	26	change_notifyfile
103	Can delete Файл	26	delete_notifyfile
104	Can view Файл	26	view_notifyfile
105	Can add Шаблон	27	add_notifytemplate
106	Can change Шаблон	27	change_notifytemplate
107	Can delete Шаблон	27	delete_notifytemplate
108	Can view Шаблон	27	view_notifytemplate
109	Can add Список пользователей для рассылки	28	add_notifyuserlist
110	Can change Список пользователей для рассылки	28	change_notifyuserlist
111	Can delete Список пользователей для рассылки	28	delete_notifyuserlist
112	Can view Список пользователей для рассылки	28	view_notifyuserlist
113	Can add Участник списка пользователей	29	add_notifyuserlistparticipant
114	Can change Участник списка пользователей	29	change_notifyuserlistparticipant
115	Can delete Участник списка пользователей	29	delete_notifyuserlistparticipant
116	Can view Участник списка пользователей	29	view_notifyuserlistparticipant
117	Can add FCM аккаунт	30	add_notifydevice
118	Can change FCM аккаунт	30	change_notifydevice
119	Can delete FCM аккаунт	30	delete_notifydevice
120	Can view FCM аккаунт	30	view_notifydevice
121	Can add SMTP аккаунт	31	add_smtpaccount
122	Can change SMTP аккаунт	31	change_smtpaccount
123	Can delete SMTP аккаунт	31	delete_smtpaccount
124	Can view SMTP аккаунт	31	view_smtpaccount
125	Can add Страница	32	add_page
126	Can change Страница	32	change_page
127	Can delete Страница	32	delete_page
128	Can view Страница	32	view_page
129	Can add Пользователь	33	add_user
130	Can change Пользователь	33	change_user
131	Can delete Пользователь	33	delete_user
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add site	6	add_site
22	Can change site	6	change_site
23	Can delete site	6	delete_site
24	Can view site	6	view_site
25	Can add FCM device	7	add_fcmdevice
26	Can change FCM device	7	change_fcmdevice
27	Can delete FCM device	7	delete_fcmdevice
28	Can view FCM device	7	view_fcmdevice
29	Can add Token	8	add_token
30	Can change Token	8	change_token
31	Can delete Token	8	delete_token
32	Can view Token	8	view_token
33	Can add token	9	add_tokenproxy
34	Can change token	9	change_tokenproxy
35	Can delete token	9	delete_tokenproxy
36	Can view token	9	view_tokenproxy
37	Can add application	10	add_application
38	Can change application	10	change_application
39	Can delete application	10	delete_application
40	Can view application	10	view_application
66	Can change nonce	17	change_nonce
67	Can delete nonce	17	delete_nonce
68	Can view nonce	17	view_nonce
69	Can add user social auth	18	add_usersocialauth
70	Can change user social auth	18	change_usersocialauth
71	Can delete user social auth	18	delete_usersocialauth
72	Can view user social auth	18	view_usersocialauth
73	Can add partial	19	add_partial
74	Can change partial	19	change_partial
75	Can delete partial	19	delete_partial
76	Can view partial	19	view_partial
77	Can add Структура страниц	20	add_basepage
78	Can change Структура страниц	20	change_basepage
79	Can delete Структура страниц	20	delete_basepage
80	Can view Структура страниц	20	view_basepage
81	Can add Пункт меню	21	add_menuitem
82	Can change Пункт меню	21	change_menuitem
83	Can delete Пункт меню	21	delete_menuitem
84	Can view Пункт меню	21	view_menuitem
85	Can add Уведомление	22	add_notify
86	Can change Уведомление	22	change_notify
87	Can delete Уведомление	22	delete_notify
88	Can view Уведомление	22	view_notify
132	Can view Пользователь	33	view_user
133	Can add Тэг фотоальбома	34	add_tagalbum
134	Can change Тэг фотоальбома	34	change_tagalbum
135	Can delete Тэг фотоальбома	34	delete_tagalbum
136	Can view Тэг фотоальбома	34	view_tagalbum
137	Can add Тэг фотографии	35	add_tagphoto
138	Can change Тэг фотографии	35	change_tagphoto
139	Can delete Тэг фотографии	35	delete_tagphoto
140	Can view Тэг фотографии	35	view_tagphoto
141	Can add Фотоальбом	36	add_photoalbum
142	Can change Фотоальбом	36	change_photoalbum
143	Can delete Фотоальбом	36	delete_photoalbum
144	Can view Фотоальбом	36	view_photoalbum
145	Can add Фотография	37	add_photo
146	Can change Фотография	37	change_photo
147	Can delete Фотография	37	delete_photo
148	Can view Фотография	37	view_photo
149	Can add Refresh Token	38	add_refreshtoken
150	Can change Refresh Token	38	change_refreshtoken
151	Can delete Refresh Token	38	delete_refreshtoken
152	Can view Refresh Token	38	view_refreshtoken
153	Can add Access Token	39	add_accesstoken
154	Can change Access Token	39	change_accesstoken
155	Can delete Access Token	39	delete_accesstoken
156	Can view Access Token	39	view_accesstoken
\.


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
12	2021-12-27 11:59:54.4+00	3	хорошая погода	1	[{"added": {}}]	34	1
13	2021-12-27 12:00:01.402+00	3	хорошая погода	1	[{"added": {}}]	36	1
14	2021-12-27 12:08:22.444+00	4	солнце	1	[{"added": {}}]	35	1
15	2021-12-27 12:09:22.631+00	2	рассвет	1	[{"added": {}}]	37	1
16	2021-12-27 12:11:54.415+00	3	солнце	1	[{"added": {}}]	37	1
17	2021-12-27 12:13:20.46+00	4	ураган	1	[{"added": {}}]	37	1
1	2021-12-22 21:44:07.238+00	1	плохая погода	1	[{"added": {}}]	34	1
2	2021-12-22 21:44:21.148+00	2	природа	1	[{"added": {}}]	34	1
3	2021-12-22 21:44:42.383+00	1	Бедствия	1	[{"added": {}}]	36	1
4	2021-12-22 21:45:12.615+00	1	природа	1	[{"added": {}}]	35	1
5	2021-12-22 21:45:20.476+00	2	Мощь	1	[{"added": {}}]	35	1
6	2021-12-22 21:45:27.777+00	3	ветер	1	[{"added": {}}]	35	1
7	2021-12-22 21:46:15.475+00	1	Торнадо	1	[{"added": {}}]	37	1
8	2021-12-22 21:46:45.47+00	1	Торнадо	2	[{"changed": {"fields": ["\\u041c\\u0438\\u043d\\u0438\\u0442\\u044e\\u0440\\u0430"]}}]	37	1
9	2021-12-22 21:48:12.737+00	1	Торнадо	2	[]	37	1
10	2021-12-27 11:58:14.446+00	2	test_user	1	[{"added": {}}]	33	1
11	2021-12-27 11:59:19.096+00	2	test_title	3		36	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	sites	site
7	fcm_django	fcmdevice
8	authtoken	token
9	authtoken	tokenproxy
10	oauth2_provider	application
11	oauth2_provider	accesstoken
12	oauth2_provider	grant
13	oauth2_provider	refreshtoken
14	oauth2_provider	idtoken
15	social_django	association
16	social_django	code
17	social_django	nonce
18	social_django	usersocialauth
19	social_django	partial
20	garpix_page	basepage
21	garpix_menu	menuitem
22	garpix_notify	notify
23	garpix_notify	notifycategory
24	garpix_notify	notifyconfig
25	garpix_notify	notifyerrorlog
26	garpix_notify	notifyfile
27	garpix_notify	notifytemplate
28	garpix_notify	notifyuserlist
29	garpix_notify	notifyuserlistparticipant
30	garpix_notify	notifydevice
31	garpix_notify	smtpaccount
32	garpixcms	page
33	user	user
34	photo_album	tagalbum
35	photo_album	tagphoto
36	photo_album	photoalbum
37	photo_album	photo
38	garpix_auth	refreshtoken
39	garpix_auth	accesstoken
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2021-12-22 21:41:46.57887+00
2	contenttypes	0002_remove_content_type_name	2021-12-22 21:41:46.600797+00
3	auth	0001_initial	2021-12-22 21:41:46.65101+00
4	auth	0002_alter_permission_name_max_length	2021-12-22 21:41:46.700227+00
5	auth	0003_alter_user_email_max_length	2021-12-22 21:41:46.720071+00
6	auth	0004_alter_user_username_opts	2021-12-22 21:41:46.733325+00
7	auth	0005_alter_user_last_login_null	2021-12-22 21:41:46.746467+00
8	auth	0006_require_contenttypes_0002	2021-12-22 21:41:46.751519+00
9	auth	0007_alter_validators_add_error_messages	2021-12-22 21:41:46.764658+00
10	auth	0008_alter_user_username_max_length	2021-12-22 21:41:46.777563+00
11	auth	0009_alter_user_last_name_max_length	2021-12-22 21:41:46.790169+00
12	auth	0010_alter_group_name_max_length	2021-12-22 21:41:46.809069+00
13	auth	0011_update_proxy_permissions	2021-12-22 21:41:46.821999+00
14	auth	0012_alter_user_first_name_max_length	2021-12-22 21:41:46.834224+00
15	user	0001_initial	2021-12-22 21:41:46.884221+00
16	admin	0001_initial	2021-12-22 21:41:46.97149+00
17	admin	0002_logentry_remove_auto_add	2021-12-22 21:41:47.00199+00
18	admin	0003_logentry_add_action_flag_choices	2021-12-22 21:41:47.018725+00
19	authtoken	0001_initial	2021-12-22 21:41:47.045426+00
20	authtoken	0002_auto_20160226_1747	2021-12-22 21:41:47.137691+00
21	authtoken	0003_tokenproxy	2021-12-22 21:41:47.145226+00
22	fcm_django	0001_initial	2021-12-22 21:41:47.180191+00
23	fcm_django	0002_auto_20160808_1645	2021-12-22 21:41:47.235145+00
24	fcm_django	0003_auto_20170313_1314	2021-12-22 21:41:47.25947+00
25	fcm_django	0004_auto_20181128_1642	2021-12-22 21:41:47.284123+00
26	fcm_django	0005_auto_20170808_1145	2021-12-22 21:41:47.327002+00
27	garpix_auth	0001_initial	2021-12-22 21:41:47.384325+00
28	sites	0001_initial	2021-12-22 21:41:47.417301+00
29	sites	0002_alter_domain_unique	2021-12-22 21:41:47.433344+00
30	garpix_page	0001_initial	2021-12-22 21:41:47.487456+00
31	garpix_menu	0001_initial	2021-12-22 21:41:47.556551+00
32	garpix_menu	0002_auto_20211216_0155	2021-12-22 21:41:47.631827+00
33	garpix_notify	0001_initial	2021-12-22 21:41:47.788646+00
34	garpix_notify	0002_auto_20211216_0155	2021-12-22 21:41:48.213202+00
35	garpixcms	0001_initial	2021-12-22 21:41:48.339754+00
36	oauth2_provider	0001_initial	2021-12-22 21:41:48.613364+00
37	oauth2_provider	0002_auto_20190406_1805	2021-12-22 21:41:48.760122+00
38	oauth2_provider	0003_auto_20201211_1314	2021-12-22 21:41:48.801513+00
39	oauth2_provider	0004_auto_20200902_2022	2021-12-22 21:41:49.051348+00
40	photo_album	0001_initial	2021-12-22 21:41:49.233837+00
41	sessions	0001_initial	2021-12-22 21:41:49.306011+00
42	default	0001_initial	2021-12-22 21:41:49.480249+00
43	social_auth	0001_initial	2021-12-22 21:41:49.482973+00
44	default	0002_add_related_name	2021-12-22 21:41:49.698556+00
45	social_auth	0002_add_related_name	2021-12-22 21:41:49.701292+00
46	default	0003_alter_email_max_length	2021-12-22 21:41:49.715744+00
47	social_auth	0003_alter_email_max_length	2021-12-22 21:41:49.719143+00
48	default	0004_auto_20160423_0400	2021-12-22 21:41:49.767017+00
49	social_auth	0004_auto_20160423_0400	2021-12-22 21:41:49.770386+00
50	social_auth	0005_auto_20160727_2333	2021-12-22 21:41:49.785976+00
51	social_django	0006_partial	2021-12-22 21:41:49.809783+00
52	social_django	0007_code_timestamp	2021-12-22 21:41:49.835522+00
53	social_django	0008_partial_timestamp	2021-12-22 21:41:49.857607+00
54	social_django	0009_auto_20191118_0520	2021-12-22 21:41:49.970161+00
55	social_django	0010_uid_db_index	2021-12-22 21:41:50.027055+00
56	social_django	0005_auto_20160727_2333	2021-12-22 21:41:50.034783+00
57	social_django	0003_alter_email_max_length	2021-12-22 21:41:50.041815+00
58	social_django	0002_add_related_name	2021-12-22 21:41:50.046054+00
59	social_django	0001_initial	2021-12-22 21:41:50.05034+00
60	social_django	0004_auto_20160423_0400	2021-12-22 21:41:50.054876+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
5etuc16pj9c0jiqjso1o0rv77suf4wmv	.eJxVjMEOwiAQBf-FsyFQoIBH734DAXZXqgaS0p5M_7026UGvb2beh4W4LiWsHecwAbsyyS6_W4r5hfUA8Iz10XhudZmnxA-Fn7TzewN8307376DEXr618kIBGZJWjlYTCFApWg9OOEEOrXRGYibUMIJwCgx6nZCGbIz1akhs2wHd5Tfq:1n09OP:5k1El5P4zNqyyWbirv88zLi0P2LOXYqmdYEHu77qEYs	2022-01-05 21:43:25.749+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: fcm_django_fcmdevice; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.fcm_django_fcmdevice (id, name, active, date_created, device_id, registration_id, type, user_id) FROM stdin;
\.


--
-- Data for Name: garpix_auth_accesstoken; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_auth_accesstoken (key, created, user_id) FROM stdin;
0b5e51830eac83fbdc0de33f0c7f55c17eb2e730	2021-12-22 21:57:17.796+00	1
\.


--
-- Data for Name: garpix_auth_refreshtoken; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_auth_refreshtoken (key, created, user_id) FROM stdin;
c9f024773f89ee73f7ccaf73003ba6c07069e90d	2021-12-22 21:57:17.806+00	1
\.


--
-- Data for Name: garpix_menu_menuitem; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_menu_menuitem (id, url, title_for_admin, title, title_ru, icon, menu_type, is_active, target_blank, created_at, updated_at, sort, lft, rght, tree_id, level, page_id, parent_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notify; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notify (id, phone, telegram_chat_id, telegram_secret, viber_chat_id, viber_secret_key, subject, text, html, email, sender_email, type, state, event, is_read, data_json, created_at, send_at, sent_at, category_id, user_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notify_files; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notify_files (id, notify_id, notifyfile_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifycategory; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifycategory (id, title, template, created_at) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifyconfig; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifyconfig (id, periodic, email_max_day_limit, email_max_hour_limit, sms_url, sms_api_id, sms_from, telegram_api_key, telegram_bot_name, telegram_welcome_text, telegram_help_text, telegram_bad_command_text, telegram_success_added_text, telegram_failed_added_text, viber_api_key, viber_bot_name, is_email_enabled, is_sms_enabled, is_push_enabled, is_telegram_enabled, is_viber_enabled, viber_success_added_text, viber_failed_added_text, viber_text_for_new_sub, viber_welcome_text) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifyerrorlog; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifyerrorlog (id, error, created_at, notify_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifyfile; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifyfile (id, file, created_at) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifytemplate; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifytemplate (id, phone, telegram_chat_id, telegram_secret, viber_chat_id, viber_secret_key, title, subject, text, html, email, type, event, created_at, is_active, send_at, category_id, user_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifytemplate_user_lists; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifytemplate_user_lists (id, notifytemplate_id, notifyuserlist_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifyuserlist; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifyuserlist (id, title, created_at, mail_to_all) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifyuserlist_user_groups; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifyuserlist_user_groups (id, notifyuserlist_id, group_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_notifyuserlistparticipant; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_notifyuserlistparticipant (id, phone, telegram_chat_id, telegram_secret, viber_chat_id, viber_secret_key, created_at, email, user_id, user_list_id) FROM stdin;
\.


--
-- Data for Name: garpix_notify_smtpaccount; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_notify_smtpaccount (id, host, port, is_use_tls, is_use_ssl, sender, username, password, timeout, is_active, email_hour_used_times, email_day_used_times, email_hour_used_date, email_day_used_date, created_at, category_id) FROM stdin;
\.


--
-- Data for Name: garpix_page_basepage; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_page_basepage (id, title, title_ru, is_active, display_on_sitemap, slug, created_at, updated_at, seo_title, seo_title_ru, seo_keywords, seo_keywords_ru, seo_description, seo_description_ru, seo_author, seo_author_ru, seo_og_type, seo_image, lft, rght, tree_id, level, parent_id, polymorphic_ctype_id) FROM stdin;
\.


--
-- Data for Name: garpix_page_basepage_sites; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpix_page_basepage_sites (id, basepage_id, site_id) FROM stdin;
\.


--
-- Data for Name: garpixcms_page; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.garpixcms_page (basepage_ptr_id, content, content_ru) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_accesstoken; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.oauth2_provider_accesstoken (id, token, expires, scope, application_id, user_id, created, updated, source_refresh_token_id, id_token_id) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_application; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.oauth2_provider_application (id, client_id, redirect_uris, client_type, authorization_grant_type, client_secret, name, user_id, skip_authorization, created, updated, algorithm) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_grant; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.oauth2_provider_grant (id, code, expires, redirect_uri, scope, application_id, user_id, created, updated, code_challenge, code_challenge_method, nonce, claims) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_idtoken; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.oauth2_provider_idtoken (id, jti, expires, scope, created, updated, application_id, user_id) FROM stdin;
\.


--
-- Data for Name: oauth2_provider_refreshtoken; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.oauth2_provider_refreshtoken (id, token, access_token_id, application_id, user_id, created, updated, revoked) FROM stdin;
\.


--
-- Data for Name: photo_album_photo; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.photo_album_photo (id, title_photo, description_photo, photo, thumbnail_photo, created_photo, album_id, author_id) FROM stdin;
1	Торнадо		author_1/Бедствия/торнадо.jpg	author_1/Бедствия/thumbnail_торнадо.jpg	2021-12-22 21:44:50+00	1	1
2	рассвет		author_1/хорошая погода/рассвет.jpg	author_1/хорошая погода/thumbnail_рассвет.jpg	2021-12-27 12:09:22.325+00	3	1
3	солнце		author_1/хорошая погода/солнце.jpg	author_1/хорошая погода/thumbnail_солнце.jpg	2021-12-27 12:11:54.324+00	3	1
4	ураган		author_1/Бедствия/ураган.jpg	author_1/Бедствия/thumbnail_ураган.jpg	2021-12-27 12:13:20.209+00	1	1
\.


--
-- Data for Name: photo_album_photo_tagPhoto; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public."photo_album_photo_tagPhoto" (id, photo_id, tagphoto_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	2	1
5	2	4
6	3	1
7	3	4
8	4	1
9	4	2
10	4	3
\.


--
-- Data for Name: photo_album_photoalbum; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.photo_album_photoalbum (id, title_album, created_album, author_id) FROM stdin;
1	Бедствия	2021-12-22 21:43:38+00	1
3	хорошая погода	2021-12-27 12:00:01.367+00	1
\.


--
-- Data for Name: photo_album_photoalbum_tagAlbum; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public."photo_album_photoalbum_tagAlbum" (id, photoalbum_id, tagalbum_id) FROM stdin;
1	1	1
2	1	2
3	3	2
4	3	3
\.


--
-- Data for Name: photo_album_tagalbum; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.photo_album_tagalbum (id, title) FROM stdin;
1	плохая погода
2	природа
3	хорошая погода
\.


--
-- Data for Name: photo_album_tagphoto; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.photo_album_tagphoto (id, title) FROM stdin;
1	природа
2	Мощь
3	ветер
4	солнце
\.


--
-- Data for Name: social_auth_association; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.social_auth_association (id, server_url, handle, secret, issued, lifetime, assoc_type) FROM stdin;
\.


--
-- Data for Name: social_auth_code; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.social_auth_code (id, email, code, verified, "timestamp") FROM stdin;
\.


--
-- Data for Name: social_auth_nonce; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.social_auth_nonce (id, server_url, "timestamp", salt) FROM stdin;
\.


--
-- Data for Name: social_auth_partial; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.social_auth_partial (id, token, next_step, backend, data, "timestamp") FROM stdin;
\.


--
-- Data for Name: social_auth_usersocialauth; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.social_auth_usersocialauth (id, provider, uid, extra_data, user_id, created, modified) FROM stdin;
\.


--
-- Data for Name: user_user; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.user_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined, phone, telegram_chat_id, telegram_secret, viber_chat_id, viber_secret_key) FROM stdin;
1	pbkdf2_sha256$216000$BXIk8KjMmTNS$JVy9rCjYJHM2ZSGQUtRKmuvoBJCk6Xg9pwGU0BqCslk=	2021-12-22 21:43:25.744+00	t	admin				t	t	2021-12-22 21:42:52.327+00			c43224a08e87427e879fafa42416a2a1		
2	pbkdf2_sha256$216000$EdbBIkdKodms$GM036TEG0YPvVThsbj8vkFe24Ihee9VeqhHoVmWzlB4=	\N	f	test_user				f	t	2021-12-27 11:58:14.042+00			a70a6c506c1541689163bce9dd9f974d		
\.


--
-- Data for Name: user_user_groups; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.user_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: user_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: user
--

COPY public.user_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 156, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 17, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 39, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 60, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- Name: fcm_django_fcmdevice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.fcm_django_fcmdevice_id_seq', 1, false);


--
-- Name: garpix_menu_menuitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_menu_menuitem_id_seq', 1, false);


--
-- Name: garpix_notify_notify_files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notify_files_id_seq', 1, false);


--
-- Name: garpix_notify_notify_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notify_id_seq', 1, false);


--
-- Name: garpix_notify_notifycategory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifycategory_id_seq', 1, false);


--
-- Name: garpix_notify_notifyconfig_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifyconfig_id_seq', 1, false);


--
-- Name: garpix_notify_notifyerrorlog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifyerrorlog_id_seq', 1, false);


--
-- Name: garpix_notify_notifyfile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifyfile_id_seq', 1, false);


--
-- Name: garpix_notify_notifytemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifytemplate_id_seq', 1, false);


--
-- Name: garpix_notify_notifytemplate_user_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifytemplate_user_lists_id_seq', 1, false);


--
-- Name: garpix_notify_notifyuserlist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifyuserlist_id_seq', 1, false);


--
-- Name: garpix_notify_notifyuserlist_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifyuserlist_user_groups_id_seq', 1, false);


--
-- Name: garpix_notify_notifyuserlistparticipant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_notifyuserlistparticipant_id_seq', 1, false);


--
-- Name: garpix_notify_smtpaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_notify_smtpaccount_id_seq', 1, false);


--
-- Name: garpix_page_basepage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_page_basepage_id_seq', 1, false);


--
-- Name: garpix_page_basepage_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.garpix_page_basepage_sites_id_seq', 1, false);


--
-- Name: oauth2_provider_accesstoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.oauth2_provider_accesstoken_id_seq', 1, false);


--
-- Name: oauth2_provider_application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.oauth2_provider_application_id_seq', 1, false);


--
-- Name: oauth2_provider_grant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.oauth2_provider_grant_id_seq', 1, false);


--
-- Name: oauth2_provider_idtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.oauth2_provider_idtoken_id_seq', 1, false);


--
-- Name: oauth2_provider_refreshtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.oauth2_provider_refreshtoken_id_seq', 1, false);


--
-- Name: photo_album_photo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.photo_album_photo_id_seq', 4, true);


--
-- Name: photo_album_photo_tagPhoto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."photo_album_photo_tagPhoto_id_seq"', 10, true);


--
-- Name: photo_album_photoalbum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.photo_album_photoalbum_id_seq', 3, true);


--
-- Name: photo_album_photoalbum_tagAlbum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public."photo_album_photoalbum_tagAlbum_id_seq"', 4, true);


--
-- Name: photo_album_tagalbum_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.photo_album_tagalbum_id_seq', 3, true);


--
-- Name: photo_album_tagphoto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.photo_album_tagphoto_id_seq', 4, true);


--
-- Name: social_auth_association_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.social_auth_association_id_seq', 1, false);


--
-- Name: social_auth_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.social_auth_code_id_seq', 1, false);


--
-- Name: social_auth_nonce_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.social_auth_nonce_id_seq', 1, false);


--
-- Name: social_auth_partial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.social_auth_partial_id_seq', 1, false);


--
-- Name: social_auth_usersocialauth_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.social_auth_usersocialauth_id_seq', 1, false);


--
-- Name: user_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.user_user_groups_id_seq', 1, false);


--
-- Name: user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.user_user_id_seq', 2, true);


--
-- Name: user_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user
--

SELECT pg_catalog.setval('public.user_user_user_permissions_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: authtoken_token authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: fcm_django_fcmdevice fcm_django_fcmdevice_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.fcm_django_fcmdevice
    ADD CONSTRAINT fcm_django_fcmdevice_pkey PRIMARY KEY (id);


--
-- Name: garpix_auth_accesstoken garpix_auth_accesstoken_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_auth_accesstoken
    ADD CONSTRAINT garpix_auth_accesstoken_pkey PRIMARY KEY (key);


--
-- Name: garpix_auth_refreshtoken garpix_auth_refreshtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_auth_refreshtoken
    ADD CONSTRAINT garpix_auth_refreshtoken_pkey PRIMARY KEY (key);


--
-- Name: garpix_menu_menuitem garpix_menu_menuitem_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_menu_menuitem
    ADD CONSTRAINT garpix_menu_menuitem_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notify_files garpix_notify_notify_fil_notify_id_notifyfile_id_0c4b8664_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify_files
    ADD CONSTRAINT garpix_notify_notify_fil_notify_id_notifyfile_id_0c4b8664_uniq UNIQUE (notify_id, notifyfile_id);


--
-- Name: garpix_notify_notify_files garpix_notify_notify_files_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify_files
    ADD CONSTRAINT garpix_notify_notify_files_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notify garpix_notify_notify_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify
    ADD CONSTRAINT garpix_notify_notify_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notify garpix_notify_notify_telegram_secret_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify
    ADD CONSTRAINT garpix_notify_notify_telegram_secret_key UNIQUE (telegram_secret);


--
-- Name: garpix_notify_notifycategory garpix_notify_notifycategory_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifycategory
    ADD CONSTRAINT garpix_notify_notifycategory_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifyconfig garpix_notify_notifyconfig_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyconfig
    ADD CONSTRAINT garpix_notify_notifyconfig_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifyerrorlog garpix_notify_notifyerrorlog_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyerrorlog
    ADD CONSTRAINT garpix_notify_notifyerrorlog_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifyfile garpix_notify_notifyfile_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyfile
    ADD CONSTRAINT garpix_notify_notifyfile_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifytemplate_user_lists garpix_notify_notifytemp_notifytemplate_id_notify_d244696d_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate_user_lists
    ADD CONSTRAINT garpix_notify_notifytemp_notifytemplate_id_notify_d244696d_uniq UNIQUE (notifytemplate_id, notifyuserlist_id);


--
-- Name: garpix_notify_notifytemplate garpix_notify_notifytemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate
    ADD CONSTRAINT garpix_notify_notifytemplate_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifytemplate garpix_notify_notifytemplate_telegram_secret_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate
    ADD CONSTRAINT garpix_notify_notifytemplate_telegram_secret_key UNIQUE (telegram_secret);


--
-- Name: garpix_notify_notifytemplate_user_lists garpix_notify_notifytemplate_user_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate_user_lists
    ADD CONSTRAINT garpix_notify_notifytemplate_user_lists_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifyuserlist_user_groups garpix_notify_notifyuser_notifyuserlist_id_group__826a2f23_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlist_user_groups
    ADD CONSTRAINT garpix_notify_notifyuser_notifyuserlist_id_group__826a2f23_uniq UNIQUE (notifyuserlist_id, group_id);


--
-- Name: garpix_notify_notifyuserlist garpix_notify_notifyuserlist_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlist
    ADD CONSTRAINT garpix_notify_notifyuserlist_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifyuserlist_user_groups garpix_notify_notifyuserlist_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlist_user_groups
    ADD CONSTRAINT garpix_notify_notifyuserlist_user_groups_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifyuserlistparticipant garpix_notify_notifyuserlistparticipant_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlistparticipant
    ADD CONSTRAINT garpix_notify_notifyuserlistparticipant_pkey PRIMARY KEY (id);


--
-- Name: garpix_notify_notifyuserlistparticipant garpix_notify_notifyuserlistparticipant_telegram_secret_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlistparticipant
    ADD CONSTRAINT garpix_notify_notifyuserlistparticipant_telegram_secret_key UNIQUE (telegram_secret);


--
-- Name: garpix_notify_smtpaccount garpix_notify_smtpaccount_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_smtpaccount
    ADD CONSTRAINT garpix_notify_smtpaccount_pkey PRIMARY KEY (id);


--
-- Name: garpix_page_basepage garpix_page_basepage_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage
    ADD CONSTRAINT garpix_page_basepage_pkey PRIMARY KEY (id);


--
-- Name: garpix_page_basepage_sites garpix_page_basepage_sites_basepage_id_site_id_5e80fdb2_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage_sites
    ADD CONSTRAINT garpix_page_basepage_sites_basepage_id_site_id_5e80fdb2_uniq UNIQUE (basepage_id, site_id);


--
-- Name: garpix_page_basepage_sites garpix_page_basepage_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage_sites
    ADD CONSTRAINT garpix_page_basepage_sites_pkey PRIMARY KEY (id);


--
-- Name: garpixcms_page garpixcms_page_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpixcms_page
    ADD CONSTRAINT garpixcms_page_pkey PRIMARY KEY (basepage_ptr_id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_id_token_id_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_id_token_id_key UNIQUE (id_token_id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_source_refresh_token_id_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_source_refresh_token_id_key UNIQUE (source_refresh_token_id);


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_token_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_token_key UNIQUE (token);


--
-- Name: oauth2_provider_application oauth2_provider_application_client_id_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_application
    ADD CONSTRAINT oauth2_provider_application_client_id_key UNIQUE (client_id);


--
-- Name: oauth2_provider_application oauth2_provider_application_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_application
    ADD CONSTRAINT oauth2_provider_application_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_grant oauth2_provider_grant_code_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_grant_code_key UNIQUE (code);


--
-- Name: oauth2_provider_grant oauth2_provider_grant_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_grant_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_idtoken oauth2_provider_idtoken_jti_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idtoken_jti_key UNIQUE (jti);


--
-- Name: oauth2_provider_idtoken oauth2_provider_idtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idtoken_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_access_token_id_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_access_token_id_key UNIQUE (access_token_id);


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_pkey PRIMARY KEY (id);


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_token_revoked_af8a5134_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_token_revoked_af8a5134_uniq UNIQUE (token, revoked);


--
-- Name: photo_album_photo photo_album_photo_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_photo
    ADD CONSTRAINT photo_album_photo_pkey PRIMARY KEY (id);


--
-- Name: photo_album_photo_tagPhoto photo_album_photo_tagPhoto_photo_id_tagphoto_id_4e3c1867_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photo_tagPhoto"
    ADD CONSTRAINT "photo_album_photo_tagPhoto_photo_id_tagphoto_id_4e3c1867_uniq" UNIQUE (photo_id, tagphoto_id);


--
-- Name: photo_album_photo_tagPhoto photo_album_photo_tagPhoto_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photo_tagPhoto"
    ADD CONSTRAINT "photo_album_photo_tagPhoto_pkey" PRIMARY KEY (id);


--
-- Name: photo_album_photoalbum photo_album_photoalbum_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_photoalbum
    ADD CONSTRAINT photo_album_photoalbum_pkey PRIMARY KEY (id);


--
-- Name: photo_album_photoalbum_tagAlbum photo_album_photoalbum_t_photoalbum_id_tagalbum_i_09e27803_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photoalbum_tagAlbum"
    ADD CONSTRAINT photo_album_photoalbum_t_photoalbum_id_tagalbum_i_09e27803_uniq UNIQUE (photoalbum_id, tagalbum_id);


--
-- Name: photo_album_photoalbum_tagAlbum photo_album_photoalbum_tagAlbum_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photoalbum_tagAlbum"
    ADD CONSTRAINT "photo_album_photoalbum_tagAlbum_pkey" PRIMARY KEY (id);


--
-- Name: photo_album_tagalbum photo_album_tagalbum_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_tagalbum
    ADD CONSTRAINT photo_album_tagalbum_pkey PRIMARY KEY (id);


--
-- Name: photo_album_tagphoto photo_album_tagphoto_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_tagphoto
    ADD CONSTRAINT photo_album_tagphoto_pkey PRIMARY KEY (id);


--
-- Name: social_auth_association social_auth_association_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_association
    ADD CONSTRAINT social_auth_association_pkey PRIMARY KEY (id);


--
-- Name: social_auth_association social_auth_association_server_url_handle_078befa2_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_association
    ADD CONSTRAINT social_auth_association_server_url_handle_078befa2_uniq UNIQUE (server_url, handle);


--
-- Name: social_auth_code social_auth_code_email_code_801b2d02_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_code
    ADD CONSTRAINT social_auth_code_email_code_801b2d02_uniq UNIQUE (email, code);


--
-- Name: social_auth_code social_auth_code_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_code
    ADD CONSTRAINT social_auth_code_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce social_auth_nonce_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_pkey PRIMARY KEY (id);


--
-- Name: social_auth_nonce social_auth_nonce_server_url_timestamp_salt_f6284463_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_nonce
    ADD CONSTRAINT social_auth_nonce_server_url_timestamp_salt_f6284463_uniq UNIQUE (server_url, "timestamp", salt);


--
-- Name: social_auth_partial social_auth_partial_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_partial
    ADD CONSTRAINT social_auth_partial_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_pkey PRIMARY KEY (id);


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_provider_uid_e6b5e668_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_provider_uid_e6b5e668_uniq UNIQUE (provider, uid);


--
-- Name: user_user_groups user_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_groups
    ADD CONSTRAINT user_user_groups_pkey PRIMARY KEY (id);


--
-- Name: user_user_groups user_user_groups_user_id_group_id_bb60391f_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_groups
    ADD CONSTRAINT user_user_groups_user_id_group_id_bb60391f_uniq UNIQUE (user_id, group_id);


--
-- Name: user_user user_user_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user
    ADD CONSTRAINT user_user_pkey PRIMARY KEY (id);


--
-- Name: user_user user_user_telegram_secret_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user
    ADD CONSTRAINT user_user_telegram_secret_key UNIQUE (telegram_secret);


--
-- Name: user_user_user_permissions user_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_user_permissions
    ADD CONSTRAINT user_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: user_user_user_permissions user_user_user_permissions_user_id_permission_id_64f4d5b8_uniq; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_user_permissions
    ADD CONSTRAINT user_user_user_permissions_user_id_permission_id_64f4d5b8_uniq UNIQUE (user_id, permission_id);


--
-- Name: user_user user_user_username_key; Type: CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user
    ADD CONSTRAINT user_user_username_key UNIQUE (username);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: fcm_django_fcmdevice_device_id_a9406c36; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX fcm_django_fcmdevice_device_id_a9406c36 ON public.fcm_django_fcmdevice USING btree (device_id);


--
-- Name: fcm_django_fcmdevice_user_id_6cdfc0a2; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX fcm_django_fcmdevice_user_id_6cdfc0a2 ON public.fcm_django_fcmdevice USING btree (user_id);


--
-- Name: garpix_auth_accesstoken_key_0e135c92_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_auth_accesstoken_key_0e135c92_like ON public.garpix_auth_accesstoken USING btree (key varchar_pattern_ops);


--
-- Name: garpix_auth_accesstoken_user_id_c8e3f165; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_auth_accesstoken_user_id_c8e3f165 ON public.garpix_auth_accesstoken USING btree (user_id);


--
-- Name: garpix_auth_refreshtoken_key_d3110647_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_auth_refreshtoken_key_d3110647_like ON public.garpix_auth_refreshtoken USING btree (key varchar_pattern_ops);


--
-- Name: garpix_auth_refreshtoken_user_id_4d1019e8; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_auth_refreshtoken_user_id_4d1019e8 ON public.garpix_auth_refreshtoken USING btree (user_id);


--
-- Name: garpix_menu_menuitem_page_id_ee746656; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_menu_menuitem_page_id_ee746656 ON public.garpix_menu_menuitem USING btree (page_id);


--
-- Name: garpix_menu_menuitem_parent_id_0fa7f5f4; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_menu_menuitem_parent_id_0fa7f5f4 ON public.garpix_menu_menuitem USING btree (parent_id);


--
-- Name: garpix_menu_menuitem_tree_id_c2fc2f46; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_menu_menuitem_tree_id_c2fc2f46 ON public.garpix_menu_menuitem USING btree (tree_id);


--
-- Name: garpix_notify_notify_category_id_0a47804b; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notify_category_id_0a47804b ON public.garpix_notify_notify USING btree (category_id);


--
-- Name: garpix_notify_notify_files_notify_id_60e05d3f; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notify_files_notify_id_60e05d3f ON public.garpix_notify_notify_files USING btree (notify_id);


--
-- Name: garpix_notify_notify_files_notifyfile_id_3caa1c8f; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notify_files_notifyfile_id_3caa1c8f ON public.garpix_notify_notify_files USING btree (notifyfile_id);


--
-- Name: garpix_notify_notify_telegram_secret_a349006b_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notify_telegram_secret_a349006b_like ON public.garpix_notify_notify USING btree (telegram_secret varchar_pattern_ops);


--
-- Name: garpix_notify_notify_user_id_2415ab78; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notify_user_id_2415ab78 ON public.garpix_notify_notify USING btree (user_id);


--
-- Name: garpix_notify_notifyerrorlog_notify_id_4fda0b60; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifyerrorlog_notify_id_4fda0b60 ON public.garpix_notify_notifyerrorlog USING btree (notify_id);


--
-- Name: garpix_notify_notifytempla_notifytemplate_id_5468a3e9; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifytempla_notifytemplate_id_5468a3e9 ON public.garpix_notify_notifytemplate_user_lists USING btree (notifytemplate_id);


--
-- Name: garpix_notify_notifytempla_notifyuserlist_id_08fe5004; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifytempla_notifyuserlist_id_08fe5004 ON public.garpix_notify_notifytemplate_user_lists USING btree (notifyuserlist_id);


--
-- Name: garpix_notify_notifytemplate_category_id_409f148a; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifytemplate_category_id_409f148a ON public.garpix_notify_notifytemplate USING btree (category_id);


--
-- Name: garpix_notify_notifytemplate_telegram_secret_bf3c5a0c_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifytemplate_telegram_secret_bf3c5a0c_like ON public.garpix_notify_notifytemplate USING btree (telegram_secret varchar_pattern_ops);


--
-- Name: garpix_notify_notifytemplate_user_id_00054b59; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifytemplate_user_id_00054b59 ON public.garpix_notify_notifytemplate USING btree (user_id);


--
-- Name: garpix_notify_notifyuser_telegram_secret_c577f741_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifyuser_telegram_secret_c577f741_like ON public.garpix_notify_notifyuserlistparticipant USING btree (telegram_secret varchar_pattern_ops);


--
-- Name: garpix_notify_notifyuserli_notifyuserlist_id_e03ca697; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifyuserli_notifyuserlist_id_e03ca697 ON public.garpix_notify_notifyuserlist_user_groups USING btree (notifyuserlist_id);


--
-- Name: garpix_notify_notifyuserlist_user_groups_group_id_2c906db3; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifyuserlist_user_groups_group_id_2c906db3 ON public.garpix_notify_notifyuserlist_user_groups USING btree (group_id);


--
-- Name: garpix_notify_notifyuserlistparticipant_user_id_d32d21eb; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifyuserlistparticipant_user_id_d32d21eb ON public.garpix_notify_notifyuserlistparticipant USING btree (user_id);


--
-- Name: garpix_notify_notifyuserlistparticipant_user_list_id_58577a9f; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_notifyuserlistparticipant_user_list_id_58577a9f ON public.garpix_notify_notifyuserlistparticipant USING btree (user_list_id);


--
-- Name: garpix_notify_smtpaccount_category_id_5e9a374c; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_notify_smtpaccount_category_id_5e9a374c ON public.garpix_notify_smtpaccount USING btree (category_id);


--
-- Name: garpix_page_basepage_parent_id_7b55a93d; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_page_basepage_parent_id_7b55a93d ON public.garpix_page_basepage USING btree (parent_id);


--
-- Name: garpix_page_basepage_polymorphic_ctype_id_dfa626b6; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_page_basepage_polymorphic_ctype_id_dfa626b6 ON public.garpix_page_basepage USING btree (polymorphic_ctype_id);


--
-- Name: garpix_page_basepage_sites_basepage_id_57ebf6fc; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_page_basepage_sites_basepage_id_57ebf6fc ON public.garpix_page_basepage_sites USING btree (basepage_id);


--
-- Name: garpix_page_basepage_sites_site_id_4e21af6e; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_page_basepage_sites_site_id_4e21af6e ON public.garpix_page_basepage_sites USING btree (site_id);


--
-- Name: garpix_page_basepage_slug_cb93d5dd; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_page_basepage_slug_cb93d5dd ON public.garpix_page_basepage USING btree (slug);


--
-- Name: garpix_page_basepage_slug_cb93d5dd_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_page_basepage_slug_cb93d5dd_like ON public.garpix_page_basepage USING btree (slug varchar_pattern_ops);


--
-- Name: garpix_page_basepage_tree_id_5882a981; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX garpix_page_basepage_tree_id_5882a981 ON public.garpix_page_basepage USING btree (tree_id);


--
-- Name: oauth2_provider_accesstoken_application_id_b22886e1; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_accesstoken_application_id_b22886e1 ON public.oauth2_provider_accesstoken USING btree (application_id);


--
-- Name: oauth2_provider_accesstoken_token_8af090f8_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_accesstoken_token_8af090f8_like ON public.oauth2_provider_accesstoken USING btree (token varchar_pattern_ops);


--
-- Name: oauth2_provider_accesstoken_user_id_6e4c9a65; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_accesstoken_user_id_6e4c9a65 ON public.oauth2_provider_accesstoken USING btree (user_id);


--
-- Name: oauth2_provider_application_client_id_03f0cc84_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_application_client_id_03f0cc84_like ON public.oauth2_provider_application USING btree (client_id varchar_pattern_ops);


--
-- Name: oauth2_provider_application_client_secret_53133678; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_application_client_secret_53133678 ON public.oauth2_provider_application USING btree (client_secret);


--
-- Name: oauth2_provider_application_client_secret_53133678_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_application_client_secret_53133678_like ON public.oauth2_provider_application USING btree (client_secret varchar_pattern_ops);


--
-- Name: oauth2_provider_application_user_id_79829054; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_application_user_id_79829054 ON public.oauth2_provider_application USING btree (user_id);


--
-- Name: oauth2_provider_grant_application_id_81923564; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_grant_application_id_81923564 ON public.oauth2_provider_grant USING btree (application_id);


--
-- Name: oauth2_provider_grant_code_49ab4ddf_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_grant_code_49ab4ddf_like ON public.oauth2_provider_grant USING btree (code varchar_pattern_ops);


--
-- Name: oauth2_provider_grant_user_id_e8f62af8; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_grant_user_id_e8f62af8 ON public.oauth2_provider_grant USING btree (user_id);


--
-- Name: oauth2_provider_idtoken_application_id_08c5ff4f; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_idtoken_application_id_08c5ff4f ON public.oauth2_provider_idtoken USING btree (application_id);


--
-- Name: oauth2_provider_idtoken_user_id_dd512b59; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_idtoken_user_id_dd512b59 ON public.oauth2_provider_idtoken USING btree (user_id);


--
-- Name: oauth2_provider_refreshtoken_application_id_2d1c311b; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_refreshtoken_application_id_2d1c311b ON public.oauth2_provider_refreshtoken USING btree (application_id);


--
-- Name: oauth2_provider_refreshtoken_user_id_da837fce; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX oauth2_provider_refreshtoken_user_id_da837fce ON public.oauth2_provider_refreshtoken USING btree (user_id);


--
-- Name: photo_album_photo_album_id_9b26a7ee; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX photo_album_photo_album_id_9b26a7ee ON public.photo_album_photo USING btree (album_id);


--
-- Name: photo_album_photo_author_id_ca6c0f34; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX photo_album_photo_author_id_ca6c0f34 ON public.photo_album_photo USING btree (author_id);


--
-- Name: photo_album_photo_tagPhoto_photo_id_8a235f5b; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX "photo_album_photo_tagPhoto_photo_id_8a235f5b" ON public."photo_album_photo_tagPhoto" USING btree (photo_id);


--
-- Name: photo_album_photo_tagPhoto_tagphoto_id_50d7a326; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX "photo_album_photo_tagPhoto_tagphoto_id_50d7a326" ON public."photo_album_photo_tagPhoto" USING btree (tagphoto_id);


--
-- Name: photo_album_photoalbum_author_id_b9925763; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX photo_album_photoalbum_author_id_b9925763 ON public.photo_album_photoalbum USING btree (author_id);


--
-- Name: photo_album_photoalbum_tagAlbum_photoalbum_id_e572c3ee; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX "photo_album_photoalbum_tagAlbum_photoalbum_id_e572c3ee" ON public."photo_album_photoalbum_tagAlbum" USING btree (photoalbum_id);


--
-- Name: photo_album_photoalbum_tagAlbum_tagalbum_id_77213a10; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX "photo_album_photoalbum_tagAlbum_tagalbum_id_77213a10" ON public."photo_album_photoalbum_tagAlbum" USING btree (tagalbum_id);


--
-- Name: social_auth_code_code_a2393167; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_code_code_a2393167 ON public.social_auth_code USING btree (code);


--
-- Name: social_auth_code_code_a2393167_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_code_code_a2393167_like ON public.social_auth_code USING btree (code varchar_pattern_ops);


--
-- Name: social_auth_code_timestamp_176b341f; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_code_timestamp_176b341f ON public.social_auth_code USING btree ("timestamp");


--
-- Name: social_auth_partial_timestamp_50f2119f; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_partial_timestamp_50f2119f ON public.social_auth_partial USING btree ("timestamp");


--
-- Name: social_auth_partial_token_3017fea3; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_partial_token_3017fea3 ON public.social_auth_partial USING btree (token);


--
-- Name: social_auth_partial_token_3017fea3_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_partial_token_3017fea3_like ON public.social_auth_partial USING btree (token varchar_pattern_ops);


--
-- Name: social_auth_usersocialauth_uid_796e51dc; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_usersocialauth_uid_796e51dc ON public.social_auth_usersocialauth USING btree (uid);


--
-- Name: social_auth_usersocialauth_uid_796e51dc_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_usersocialauth_uid_796e51dc_like ON public.social_auth_usersocialauth USING btree (uid varchar_pattern_ops);


--
-- Name: social_auth_usersocialauth_user_id_17d28448; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX social_auth_usersocialauth_user_id_17d28448 ON public.social_auth_usersocialauth USING btree (user_id);


--
-- Name: user_user_groups_group_id_c57f13c0; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_user_groups_group_id_c57f13c0 ON public.user_user_groups USING btree (group_id);


--
-- Name: user_user_groups_user_id_13f9a20d; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_user_groups_user_id_13f9a20d ON public.user_user_groups USING btree (user_id);


--
-- Name: user_user_telegram_secret_b4486ec7_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_user_telegram_secret_b4486ec7_like ON public.user_user USING btree (telegram_secret varchar_pattern_ops);


--
-- Name: user_user_user_permissions_permission_id_ce49d4de; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_user_user_permissions_permission_id_ce49d4de ON public.user_user_user_permissions USING btree (permission_id);


--
-- Name: user_user_user_permissions_user_id_31782f58; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_user_user_permissions_user_id_31782f58 ON public.user_user_user_permissions USING btree (user_id);


--
-- Name: user_user_username_e2bdfe0c_like; Type: INDEX; Schema: public; Owner: user
--

CREATE INDEX user_user_username_e2bdfe0c_like ON public.user_user USING btree (username varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token authtoken_token_user_id_35299eff_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fcm_django_fcmdevice fcm_django_fcmdevice_user_id_6cdfc0a2_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.fcm_django_fcmdevice
    ADD CONSTRAINT fcm_django_fcmdevice_user_id_6cdfc0a2_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_auth_accesstoken garpix_auth_accesstoken_user_id_c8e3f165_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_auth_accesstoken
    ADD CONSTRAINT garpix_auth_accesstoken_user_id_c8e3f165_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_auth_refreshtoken garpix_auth_refreshtoken_user_id_4d1019e8_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_auth_refreshtoken
    ADD CONSTRAINT garpix_auth_refreshtoken_user_id_4d1019e8_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_menu_menuitem garpix_menu_menuitem_page_id_ee746656_fk_garpix_pa; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_menu_menuitem
    ADD CONSTRAINT garpix_menu_menuitem_page_id_ee746656_fk_garpix_pa FOREIGN KEY (page_id) REFERENCES public.garpix_page_basepage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_menu_menuitem garpix_menu_menuitem_parent_id_0fa7f5f4_fk_garpix_me; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_menu_menuitem
    ADD CONSTRAINT garpix_menu_menuitem_parent_id_0fa7f5f4_fk_garpix_me FOREIGN KEY (parent_id) REFERENCES public.garpix_menu_menuitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notify garpix_notify_notify_category_id_0a47804b_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify
    ADD CONSTRAINT garpix_notify_notify_category_id_0a47804b_fk_garpix_no FOREIGN KEY (category_id) REFERENCES public.garpix_notify_notifycategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifytemplate garpix_notify_notify_category_id_409f148a_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate
    ADD CONSTRAINT garpix_notify_notify_category_id_409f148a_fk_garpix_no FOREIGN KEY (category_id) REFERENCES public.garpix_notify_notifycategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifyuserlist_user_groups garpix_notify_notify_group_id_2c906db3_fk_auth_grou; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlist_user_groups
    ADD CONSTRAINT garpix_notify_notify_group_id_2c906db3_fk_auth_grou FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifyerrorlog garpix_notify_notify_notify_id_4fda0b60_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyerrorlog
    ADD CONSTRAINT garpix_notify_notify_notify_id_4fda0b60_fk_garpix_no FOREIGN KEY (notify_id) REFERENCES public.garpix_notify_notify(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notify_files garpix_notify_notify_notify_id_60e05d3f_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify_files
    ADD CONSTRAINT garpix_notify_notify_notify_id_60e05d3f_fk_garpix_no FOREIGN KEY (notify_id) REFERENCES public.garpix_notify_notify(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notify_files garpix_notify_notify_notifyfile_id_3caa1c8f_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify_files
    ADD CONSTRAINT garpix_notify_notify_notifyfile_id_3caa1c8f_fk_garpix_no FOREIGN KEY (notifyfile_id) REFERENCES public.garpix_notify_notifyfile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifytemplate_user_lists garpix_notify_notify_notifytemplate_id_5468a3e9_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate_user_lists
    ADD CONSTRAINT garpix_notify_notify_notifytemplate_id_5468a3e9_fk_garpix_no FOREIGN KEY (notifytemplate_id) REFERENCES public.garpix_notify_notifytemplate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifytemplate_user_lists garpix_notify_notify_notifyuserlist_id_08fe5004_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate_user_lists
    ADD CONSTRAINT garpix_notify_notify_notifyuserlist_id_08fe5004_fk_garpix_no FOREIGN KEY (notifyuserlist_id) REFERENCES public.garpix_notify_notifyuserlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifyuserlist_user_groups garpix_notify_notify_notifyuserlist_id_e03ca697_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlist_user_groups
    ADD CONSTRAINT garpix_notify_notify_notifyuserlist_id_e03ca697_fk_garpix_no FOREIGN KEY (notifyuserlist_id) REFERENCES public.garpix_notify_notifyuserlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notify garpix_notify_notify_user_id_2415ab78_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notify
    ADD CONSTRAINT garpix_notify_notify_user_id_2415ab78_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifyuserlistparticipant garpix_notify_notify_user_id_d32d21eb_fk_user_user; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlistparticipant
    ADD CONSTRAINT garpix_notify_notify_user_id_d32d21eb_fk_user_user FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifyuserlistparticipant garpix_notify_notify_user_list_id_58577a9f_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifyuserlistparticipant
    ADD CONSTRAINT garpix_notify_notify_user_list_id_58577a9f_fk_garpix_no FOREIGN KEY (user_list_id) REFERENCES public.garpix_notify_notifyuserlist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_notifytemplate garpix_notify_notifytemplate_user_id_00054b59_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_notifytemplate
    ADD CONSTRAINT garpix_notify_notifytemplate_user_id_00054b59_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_notify_smtpaccount garpix_notify_smtpac_category_id_5e9a374c_fk_garpix_no; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_notify_smtpaccount
    ADD CONSTRAINT garpix_notify_smtpac_category_id_5e9a374c_fk_garpix_no FOREIGN KEY (category_id) REFERENCES public.garpix_notify_notifycategory(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_page_basepage_sites garpix_page_basepage_basepage_id_57ebf6fc_fk_garpix_pa; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage_sites
    ADD CONSTRAINT garpix_page_basepage_basepage_id_57ebf6fc_fk_garpix_pa FOREIGN KEY (basepage_id) REFERENCES public.garpix_page_basepage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_page_basepage garpix_page_basepage_parent_id_7b55a93d_fk_garpix_pa; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage
    ADD CONSTRAINT garpix_page_basepage_parent_id_7b55a93d_fk_garpix_pa FOREIGN KEY (parent_id) REFERENCES public.garpix_page_basepage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_page_basepage garpix_page_basepage_polymorphic_ctype_id_dfa626b6_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage
    ADD CONSTRAINT garpix_page_basepage_polymorphic_ctype_id_dfa626b6_fk_django_co FOREIGN KEY (polymorphic_ctype_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpix_page_basepage_sites garpix_page_basepage_sites_site_id_4e21af6e_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpix_page_basepage_sites
    ADD CONSTRAINT garpix_page_basepage_sites_site_id_4e21af6e_fk_django_site_id FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: garpixcms_page garpixcms_page_basepage_ptr_id_7d163ae4_fk_garpix_pa; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.garpixcms_page
    ADD CONSTRAINT garpixcms_page_basepage_ptr_id_7d163ae4_fk_garpix_pa FOREIGN KEY (basepage_ptr_id) REFERENCES public.garpix_page_basepage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_acce_application_id_b22886e1_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_acce_application_id_b22886e1_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_acce_id_token_id_85db651b_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_acce_id_token_id_85db651b_fk_oauth2_pr FOREIGN KEY (id_token_id) REFERENCES public.oauth2_provider_idtoken(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_acce_source_refresh_token_e66fbc72_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_acce_source_refresh_token_e66fbc72_fk_oauth2_pr FOREIGN KEY (source_refresh_token_id) REFERENCES public.oauth2_provider_refreshtoken(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_accesstoken oauth2_provider_accesstoken_user_id_6e4c9a65_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_accesstoken
    ADD CONSTRAINT oauth2_provider_accesstoken_user_id_6e4c9a65_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_application oauth2_provider_application_user_id_79829054_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_application
    ADD CONSTRAINT oauth2_provider_application_user_id_79829054_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_grant oauth2_provider_gran_application_id_81923564_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_gran_application_id_81923564_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_grant oauth2_provider_grant_user_id_e8f62af8_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_grant
    ADD CONSTRAINT oauth2_provider_grant_user_id_e8f62af8_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_idtoken oauth2_provider_idto_application_id_08c5ff4f_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idto_application_id_08c5ff4f_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_idtoken oauth2_provider_idtoken_user_id_dd512b59_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_idtoken
    ADD CONSTRAINT oauth2_provider_idtoken_user_id_dd512b59_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refr_access_token_id_775e84e8_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refr_access_token_id_775e84e8_fk_oauth2_pr FOREIGN KEY (access_token_id) REFERENCES public.oauth2_provider_accesstoken(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refr_application_id_2d1c311b_fk_oauth2_pr; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refr_application_id_2d1c311b_fk_oauth2_pr FOREIGN KEY (application_id) REFERENCES public.oauth2_provider_application(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: oauth2_provider_refreshtoken oauth2_provider_refreshtoken_user_id_da837fce_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.oauth2_provider_refreshtoken
    ADD CONSTRAINT oauth2_provider_refreshtoken_user_id_da837fce_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_album_photo photo_album_photo_album_id_9b26a7ee_fk_photo_alb; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_photo
    ADD CONSTRAINT photo_album_photo_album_id_9b26a7ee_fk_photo_alb FOREIGN KEY (album_id) REFERENCES public.photo_album_photoalbum(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_album_photo photo_album_photo_author_id_ca6c0f34_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_photo
    ADD CONSTRAINT photo_album_photo_author_id_ca6c0f34_fk_user_user_id FOREIGN KEY (author_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_album_photo_tagPhoto photo_album_photo_ta_photo_id_8a235f5b_fk_photo_alb; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photo_tagPhoto"
    ADD CONSTRAINT photo_album_photo_ta_photo_id_8a235f5b_fk_photo_alb FOREIGN KEY (photo_id) REFERENCES public.photo_album_photo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_album_photo_tagPhoto photo_album_photo_ta_tagphoto_id_50d7a326_fk_photo_alb; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photo_tagPhoto"
    ADD CONSTRAINT photo_album_photo_ta_tagphoto_id_50d7a326_fk_photo_alb FOREIGN KEY (tagphoto_id) REFERENCES public.photo_album_tagphoto(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_album_photoalbum_tagAlbum photo_album_photoalb_photoalbum_id_e572c3ee_fk_photo_alb; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photoalbum_tagAlbum"
    ADD CONSTRAINT photo_album_photoalb_photoalbum_id_e572c3ee_fk_photo_alb FOREIGN KEY (photoalbum_id) REFERENCES public.photo_album_photoalbum(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_album_photoalbum_tagAlbum photo_album_photoalb_tagalbum_id_77213a10_fk_photo_alb; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public."photo_album_photoalbum_tagAlbum"
    ADD CONSTRAINT photo_album_photoalb_tagalbum_id_77213a10_fk_photo_alb FOREIGN KEY (tagalbum_id) REFERENCES public.photo_album_tagalbum(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: photo_album_photoalbum photo_album_photoalbum_author_id_b9925763_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.photo_album_photoalbum
    ADD CONSTRAINT photo_album_photoalbum_author_id_b9925763_fk_user_user_id FOREIGN KEY (author_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: social_auth_usersocialauth social_auth_usersocialauth_user_id_17d28448_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.social_auth_usersocialauth
    ADD CONSTRAINT social_auth_usersocialauth_user_id_17d28448_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_user_groups user_user_groups_group_id_c57f13c0_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_groups
    ADD CONSTRAINT user_user_groups_group_id_c57f13c0_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_user_groups user_user_groups_user_id_13f9a20d_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_groups
    ADD CONSTRAINT user_user_groups_user_id_13f9a20d_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_user_user_permissions user_user_user_permi_permission_id_ce49d4de_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_user_permissions
    ADD CONSTRAINT user_user_user_permi_permission_id_ce49d4de_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_user_user_permissions user_user_user_permissions_user_id_31782f58_fk_user_user_id; Type: FK CONSTRAINT; Schema: public; Owner: user
--

ALTER TABLE ONLY public.user_user_user_permissions
    ADD CONSTRAINT user_user_user_permissions_user_id_31782f58_fk_user_user_id FOREIGN KEY (user_id) REFERENCES public.user_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

