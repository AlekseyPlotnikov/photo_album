from garpixcms.settings import *  # noqa

INSTALLED_APPS += [  # noqa
    'photo_album',
    'rest_framework_social_oauth2',
    'api',
    'garpix_auth',
]
API_URL = 'api/v1'
