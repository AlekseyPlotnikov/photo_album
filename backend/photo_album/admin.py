from django.contrib import admin
from .models import Photo, TagPhoto, TagAlbum, PhotoAlbum


@admin.register(PhotoAlbum)
class PhotoAlbumAdmin(admin.ModelAdmin):
    list_display = ["id", "title_album", "author", "created_album"]
    readonly_fields = ('created_album',)


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    list_display = ["id", "title_photo", "author", "album", "created_photo", ]
    fields = ("title_photo", "description_photo",
              "author", "album", "tagPhoto", "photo", "thumbnail_photo", "created_photo")
    readonly_fields = ('created_photo', "thumbnail_photo",)

    def save_model(self, request, obj, form, change):
        if request.FILES:
            obj.thumbnail_photo = request.FILES.get('photo')
        super().save_model(request, obj, form, change)


admin.site.register(TagAlbum)
admin.site.register(TagPhoto)
