from django.core.exceptions import ValidationError
from django.db import models
from garpixcms import settings
from django.utils.timezone import now
from PIL import Image


class TagAlbum(models.Model):
    title = models.CharField(max_length=100, verbose_name='Тэг альбома')

    def __str__(self):
        return self.title

    def name_related(self):
        return self.tagAlbum.all()

    class Meta:
        verbose_name = "Тэг фотоальбома"
        verbose_name_plural = "Тэги фотоальбомов"


class PhotoAlbum(models.Model):
    title_album = models.CharField(max_length=100, verbose_name='Название альбома', blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='автор альбома')
    created_album = models.DateTimeField(verbose_name='Дата создания фльбома', default=now)
    tagAlbum = models.ManyToManyField(TagAlbum, related_name='tagAlbum', verbose_name='Тэг фотоальбома', blank=True,
                                      null=True)

    def __str__(self):
        return self.title_album

    def get_photo_count(self):
        return self.photo_set.count()

    class Meta:
        verbose_name = 'Фотоальбом'
        verbose_name_plural = 'Фотоальбомы'
        ordering = ('-created_album',)


class TagPhoto(models.Model):
    title = models.CharField(max_length=100, verbose_name='Тэг фото')

    def __str__(self):
        return self.title

    def name_related(self):
        return self.tagPhoto.all()

    class Meta:
        verbose_name = "Тэг фотографии"
        verbose_name_plural = "Тэги фотографий"


def directory_path(instance, filename):
    return 'author_{0}/{1}/{2}'.format(instance.author.id, instance.album.title_album, filename)


def directory_path_thumbnail(instance, filename):
    return 'author_{0}/{1}/thumbnail_{2}'.format(instance.author.id, instance.album.title_album, filename)


class Photo(models.Model):
    title_photo = models.CharField(max_length=100, verbose_name='Название фотографии', blank=True, null=True)
    description_photo = models.TextField(blank=True, verbose_name='Описание фотографии', null=True)
    photo = models.ImageField(verbose_name='Фото', blank=True, upload_to=directory_path)
    thumbnail_photo = models.ImageField(verbose_name='минитюра', blank=True, upload_to=directory_path_thumbnail)
    created_photo = models.DateTimeField(verbose_name='Дата добавления фото', default=now)
    tagPhoto = models.ManyToManyField(TagPhoto, related_name='tagPhoto', verbose_name='Тэг фотографии', blank=True,
                                      null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                               verbose_name='Пользователь, загрузивший фото')
    album = models.ForeignKey(PhotoAlbum, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        if self.photo.name:
            if 'content_type' in dir(self.photo.file):
                photo = self.photo.file
                content_allow = ('image/jpeg', 'image/jpg', 'image/png')
                content_type = photo.content_type
                if photo.size > 5 * 1024 * 1024:
                    raise ValidationError("Файл слишком большой. Максимальный размер файла 5 Mb.")
                if content_type not in content_allow:
                    raise ValidationError('Не верный формат. Возможны только png, jpg, jpeg.')
        else:
            self.thumbnail_photo.delete()

        super().save(*args, **kwargs)
        if self.thumbnail_photo.name:
            thumbnail = Image.open(self.thumbnail_photo.path)
            width, height = thumbnail.size
            fixed = 150
            if width >= height:
                percent = (float(height) / float(width))
                new_size = int((float(fixed) * float(percent)))
                thumbnail_photo = thumbnail.resize((fixed, new_size))
            else:
                percent = (float(width) / float(height))
                other_size = int((float(fixed) * float(percent)))
                thumbnail_photo = thumbnail.resize((other_size, fixed))
            thumbnail_photo.save(self.thumbnail_photo.path)

    def __str__(self):
        return self.title_photo

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотограции'
        ordering = ('-created_photo',)
