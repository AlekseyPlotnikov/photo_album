from django.contrib.auth import get_user_model
from rest_framework import serializers
from photo_album.models import TagAlbum, TagPhoto, Photo, PhotoAlbum

UserModel = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = UserModel.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )

        return user

    class Meta:
        model = UserModel
        fields = ("id", "username", "password",)


class TagAlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagAlbum
        fields = '__all__'


class TagPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TagPhoto
        fields = "__all__"


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = "__all__"
        read_only_fields = ['created_photo']
        extra_kwargs = {'author': {'required': False}}


class PhotoAlbumSerializer(serializers.ModelSerializer):
    photo_count = serializers.SerializerMethodField('get_photo_count')

    def get_photo_count(self, obj):
        return obj.photo_set.count()

    class Meta:
        model = PhotoAlbum
        fields = '__all__'
        read_only_fields = ['created_album', 'photo_count']
        extra_kwargs = {'author': {'required': False}}
