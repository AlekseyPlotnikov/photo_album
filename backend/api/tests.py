from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.test import APIClient
from photo_album.models import Photo, PhotoAlbum
from django.core.exceptions import ValidationError

User = get_user_model()


class TestTagAlbumApi(TestCase):
    fixtures = ['./fixtures/db.json']

    def setUp(self):
        self.token = "Bearer 0b5e51830eac83fbdc0de33f0c7f55c17eb2e730"
        self.username = "admin"
        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION=self.token)

    def test_TagAlbum_get(self):
        response = self.api_client.get(reverse('api:photo-album-tags'))
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, [201, 300, 400, 500])
        self.assertEqual(response.json().get('tag_album')[0], {'id': 1, 'title': 'плохая погода'})
        self.assertNotEqual(response.json().get('tag_album')[0], {'id': 1, 'title': 'test2'})

    def test_TagAlbum_post(self):
        data = {"tag_album": {"title": "new_tag"}}
        response = self.api_client.post(reverse('api:photo-album-tags'), data=data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, [201, 300, 400, 500])
        response = self.api_client.get(reverse('api:photo-album-tags'), format='json')
        self.assertEqual(response.json().get('tag_album')[3]["title"], "new_tag")


class TestTagPhotoApi(TestCase):
    fixtures = ['./fixtures/db.json']

    def setUp(self):
        self.token = "Bearer 0b5e51830eac83fbdc0de33f0c7f55c17eb2e730"
        self.username = "admin"
        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION=self.token)

    def test_TagPhoto_get(self):
        response = self.api_client.get(reverse('api:photo-tags'))
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, [201, 300, 400, 500])
        self.assertEqual(response.json().get('tag_photo')[0], {'id': 1, 'title': 'природа'})
        self.assertNotEqual(response.json().get('tag_photo')[0], {'id': 1, 'title': 'test2'})

    def test_TagPhoto_post(self):
        data = {"tag_photo": {"title": "new_tag"}}
        response = self.api_client.post(reverse('api:photo-tags'), data=data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, [201, 300, 400, 500])
        response = self.api_client.get(reverse('api:photo-tags'), format='json')
        self.assertEqual(response.json().get('tag_photo')[4]["title"], "new_tag")


class TestPhotoAlbumApi(TestCase):
    fixtures = ['./fixtures/db.json']

    def setUp(self):
        self.token = "Bearer 0b5e51830eac83fbdc0de33f0c7f55c17eb2e730"
        self.username = "admin"
        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION=self.token)

    def test_photo_album_get(self):
        response = self.api_client.get(reverse('api:current-user'), HTTP_ACCEPT='application/json', )
        self.assertEqual(response.json().get('username'), self.username)
        response = self.api_client.get(reverse('api:photo-album'), HTTP_ACCEPT='application/json')
        albums = response.json().get("photoAlbums")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(albums), 2)
        self.assertEqual(len(albums), len(PhotoAlbum.objects.filter(author__username=self.username)))
        response = self.api_client.get(reverse('api:photo-album-pk', args=[1]), HTTP_ACCEPT='application/json')
        album1 = response.json().get("PhotoAlbum")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(album1.get("id"), 1)
        response = self.api_client.get(reverse('api:photo-album') + "?ordering=id", HTTP_ACCEPT='application/json')
        albums_id = response.json().get("photoAlbums")
        self.assertEqual(response.status_code, 200)
        response = self.api_client.get(reverse('api:photo-album') + "?ordering=-id", HTTP_ACCEPT='application/json')
        albums_sort = response.json().get("photoAlbums")
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(albums_id, albums_sort)
        response = self.api_client.get(reverse('api:photo-album') + "?tagAlbum=1", HTTP_ACCEPT='application/json')
        albums_tag1 = response.json().get("photoAlbums")
        self.assertEqual(response.status_code, 200)
        response = self.api_client.get(reverse('api:photo-album') + "?tagAlbum=3", HTTP_ACCEPT='application/json')
        albums_tag3 = response.json().get("photoAlbums")
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(albums_tag1, albums_tag3)
        response = self.api_client.get(reverse('api:photo-album') + "?tagAlbum=2", HTTP_ACCEPT='application/json')
        albums_tag2 = response.json().get("photoAlbums")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(albums_tag2, albums)
        response = self.api_client.get(reverse('api:photo-album-pk', args=[4]), HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, 404)

    def test_photo_album_post(self):
        data = {"photoAlbum": {'title_album': 'погода', 'tagAlbum': [1, 2, 3]}}
        response = self.api_client.post(reverse('api:photo-album'), data=data, format='json')
        album_new = PhotoAlbum.objects.get(pk=int(response.json()["id"]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(album_new.title_album, data["photoAlbum"]["title_album"])
        self.assertNotEqual(album_new.tagAlbum, data["photoAlbum"]["tagAlbum"])

    def test_photo_album_put(self):
        album = self.api_client.get(reverse('api:photo-album-pk', args=[1]), format="json")
        album_old = album.json()['PhotoAlbum']
        data = {"photoAlbum": {'title_album': 'Бедствия', 'tagAlbum': [1, 2, 3, 4]}}
        response = self.api_client.put(reverse('api:photo-album-pk', args=[1]), data=data, format='json')
        self.assertEqual(response.status_code, 200)
        album_new = PhotoAlbum.objects.get(pk=1)
        self.assertEqual(album_new.title_album, data["photoAlbum"]["title_album"], album_old["title_album"])
        self.assertNotEqual(album_new.tagAlbum, album_old["tagAlbum"])
        self.assertEqual(album_new.title_album, data["photoAlbum"]["title_album"])
        response = self.api_client.put(reverse('api:photo-album'), data=data, format='json')
        self.assertEqual(response.status_code, 405)
        response = self.api_client.put(reverse('api:photo-album-pk', args=[4]), format='json')
        self.assertEqual(response.status_code, 404)

    def test_photo_album_delete(self):
        response = self.api_client.delete(reverse('api:photo-album-pk', args=[1]), format='json')
        self.assertEqual(response.status_code, 204)
        deleted = self.api_client.get(reverse('api:photo-album-pk', args=[1]), format='json')
        self.assertEqual(deleted.status_code, 404)
        response = self.api_client.delete(reverse('api:photo-album-pk', args=[4]), format='json')
        self.assertEqual(response.status_code, 404)


class TestPhotoApi(TestCase):
    fixtures = ['./fixtures/db.json']

    def setUp(self):
        self.token = "Bearer 0b5e51830eac83fbdc0de33f0c7f55c17eb2e730"
        self.username = "admin"
        self.api_client = APIClient()
        self.api_client.credentials(HTTP_AUTHORIZATION=self.token)

    def test_photo_get(self):
        response = self.api_client.get(reverse('api:photo'), format='json')
        photos = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(photos.get("photos")), len(Photo.objects.filter(author__username=self.username)))
        self.assertEqual(len(photos.get("photos")), response.json()["count"])
        response = self.api_client.get(reverse('api:photo-pk', args=[1]), format='json')
        photo = response.json().get("photo")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(photo.get("id"), 1)
        response = self.api_client.get(reverse('api:photo') + "?tagPhoto=4", format='json')
        photo_tags = response.json()
        self.assertGreaterEqual(photos.get("count"), photo_tags.get("count"))
        response = self.api_client.get(reverse('api:photo-album-pk', args=[1]), format='json')
        photos = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(photos['PhotoAlbum']['photo_count'], len(Photo.objects.filter(album__id=1)))
        response = self.api_client.get(reverse('api:photo-pk', args=[5]), HTTP_ACCEPT='application/json')
        self.assertEqual(response.status_code, 404)

    def test_photo_post(self):
        with open('./fixtures/штиль_тест.jpeg', 'rb') as photo:
            data = {'data': '{"photos": {"title_photo": "calm_test", "description_photo": "photo_test",'
                            '"tagPhoto": [2], "album": 1}}', 'media': photo}
            response = self.api_client.post(reverse('api:photo'), data=data, format='multipart')
        id = response.json().get("id")
        photo = Photo.objects.get(pk=id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(photo.title_photo, "calm_test")
        self.assertEqual(photo.album.author.username, self.username)
        with self.assertRaises(ValidationError):
            with open('./fixtures/большое_фото.jpeg', 'rb') as photo:
                data = {'data': '{"photos": {"title_photo": "large_photo",'
                                '"description_photo": "large_photo_test", "tagPhoto": [2], "album": 1}}', 'media': photo}
                response = self.api_client.post(reverse('api:photo'), data=data, format='multipart')
                self.assertEqual(response.status_code, 400)
        with self.assertRaises(ValidationError):
            with open('./fixtures/4RNk.gif', 'rb') as photo:
                data = {'data': '{"photos": {"title_photo": "gif_test",'
                                '"description": "gif_test",  "tagPhoto": [2], "album": 1}}', 'media': photo}
                response = self.api_client.post(reverse('api:photo'), data=data, format='multipart')
                self.assertEqual(response.status_code, 400)

    def test_photo_put(self):
        photo = self.api_client.get(reverse('api:photo-pk', args=[1]), format='json')
        photo_old = photo.json()["photo"]
        data = {"title_photo": "test_photo_put", "album": 3, 'tagPhoto': [1, 2]}
        response = self.api_client.put(reverse('api:photo-pk', args=[1]), data=data, format='json')
        photo_new = Photo.objects.get(pk=1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(photo_new.title_photo, data["title_photo"])
        self.assertNotEqual(photo_new.title_photo, photo_old["title_photo"])
        self.assertNotEqual(photo_new.tagPhoto, photo_old["tagPhoto"])
        self.assertEqual(photo_new.album.author.username, self.username)
        response = self.api_client.put(reverse('api:photo-pk', args=[9]), data=data, format='json')
        self.assertEqual(response.status_code, 404)

    def test_photo_delete(self):
        response = self.api_client.delete(reverse('api:photo-pk', args=[4]), format='json')
        self.assertEqual(response.status_code, 204)
        response = self.api_client.get(reverse('api:photo-pk', args=[4]), format='json')
        self.assertEqual(response.status_code, 404)
        response = self.api_client.delete(reverse('api:photo-pk', args=[4]), format='json')
        self.assertEqual(response.status_code, 404)
