from django.contrib.auth import get_user_model
from django.http import Http404
from rest_framework import permissions, status
from rest_framework.exceptions import PermissionDenied, MethodNotAllowed
from rest_framework.generics import CreateAPIView
from rest_framework.parsers import JSONParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from photo_album.models import TagAlbum, TagPhoto, Photo, PhotoAlbum
from .serializers import UserSerializer, PhotoSerializer, TagPhotoSerializer, TagAlbumSerializer, PhotoAlbumSerializer
from .utils import MultipartJsonParser


class CreateUserView(CreateAPIView):
    model = get_user_model()
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer


class CurrentUserView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return Response({
                'username': request.user.username,
            })
        return Response({'status': 'failed'}, status=401)


class TagAlbumModelView(APIView):
    def get(self, request):
        tag_album = TagAlbum.objects.all()
        serializer = TagAlbumSerializer(tag_album, many=True)
        return Response({'tag_album': serializer.data})

    def post(self, request):
        tag_album = request.data.get('tag_album')
        serializer = TagAlbumSerializer(data=tag_album)
        if serializer.is_valid(raise_exception=True):
            tag_album_inst = serializer.save()
        return Response({'success': f'Tag album"{tag_album_inst.title}" created successfully'})


class TagPhotoModelView(APIView):
    def get(self, request):
        tag_photo = TagPhoto.objects.all()
        serializer = TagPhotoSerializer(tag_photo, many=True)
        return Response({'tag_photo': serializer.data})

    def post(self, request):
        tag_photo = request.data.get('tag_photo')
        serializer = TagPhotoSerializer(data=tag_photo)
        if serializer.is_valid(raise_exception=True):
            tag_photo_inst = serializer.save()
        return Response({'success': f'Tag album"{tag_photo_inst.title}" created successfully'})


class PhotoAlbumModelView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self, pk, request):
        try:
            photoAlbum = PhotoAlbum.objects.get(pk=pk)
            if photoAlbum.author.id != request.user.id:
                raise PermissionDenied({"message": "You don't have permission to access"})
            else:
                return photoAlbum
        except PhotoAlbum.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None, *args, **kwargs):
        if pk:
            photoAlbum = self.get_object(pk, request)
            serializer = PhotoAlbumSerializer(photoAlbum, context={"request": request})
            return Response({"PhotoAlbum": serializer.data})
        else:
            photoAlbums = PhotoAlbum.objects.filter(author=request.user.id)
            params = request.query_params.get("ordering")
            tags = request.query_params.get("tagAlbum")
            if tags:
                tags = map(int, request.query_params.get("tagAlbum").split(","))
                for tag in tags:
                    photoAlbums = photoAlbums.filter(tagAlbum=tag).distinct()
            if params:
                if params == "count":
                    photoAlbums = sorted(photoAlbums, key=lambda x: x.get_count_photo())
                elif params == "-count":
                    photoAlbums = sorted(photoAlbums, key=lambda x: x.get_count_photo())
                else:
                    photoAlbums = photoAlbums.order_by(params)
        serializer = PhotoAlbumSerializer(photoAlbums, many=True, context={'request': request})
        return Response({'photoAlbums': serializer.data})

    def post(self, request):
        if request.user.is_authenticated:
            photoAlbum = request.data.get('photoAlbum')
            photoAlbum["author"] = request.user.id
            serializer = PhotoAlbumSerializer(data=photoAlbum)
            if serializer.is_valid(raise_exception=True):
                photoAlbum_inst = serializer.save()
            return Response({"success": f"PhotoAlbum '{photoAlbum_inst.title_album}' created successfully",
                             "id": photoAlbum_inst.id})
        return Response({'status': 'failed'}, status=401)

    def put(self, request, pk=None, format=None):
        if pk:
            photoAlbum = self.get_object(pk, request)
            serializer = PhotoAlbumSerializer(photoAlbum, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"success": "PhotoAlbum update", "update_photoAlbum": serializer.data})
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        raise MethodNotAllowed("put")

    def delete(self, request, pk=None, format=None):
        if pk:
            photoAlbum = self.get_object(pk, request)
            photoAlbum.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        raise MethodNotAllowed("delete")


class PhotoModelView(APIView):
    permission_classes = (permissions.IsAuthenticated,)
    parser_classes = (MultipartJsonParser, JSONParser)

    def get_object(self, pk, request):
        try:
            photo = Photo.objects.get(pk=pk)
            if photo.author.id != request.user.id:
                raise PermissionDenied({"message": "You don't have permission to access"})
            else:
                return photo
        except Photo.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            photo = self.get_object(pk, request)
            serializer = PhotoSerializer(photo, context={"request": request})
            return Response({'photo': serializer.data})
        else:
            photos = Photo.objects.filter(author=request.user.id)
            params = request.query_params.get("ordering")
            tags = request.query_params.get("tagPhoto")
            if tags:
                tags = map(int, request.query_params.get("tagPhoto").split(","))
                for tag in tags:
                    photos = photos.filter(tagPhoto__id=tag).distinct()
            if params:
                photos = photos.order_by(params)
            serializer = PhotoSerializer(photos, context={"request": request}, many=True)
            return Response({'count': len(serializer.data), 'photos': serializer.data})

    def post(self, request, format=None, *args, **kwargs):
        if request.user.is_authenticated:
            photo = request.data.dict().get('photos')
            photo["author"] = request.user.id
            photo["photo"] = request.data.get('media')
            photo["thumbnail_photo"] = request.data.get('media')
            serializer = PhotoSerializer(data=photo)
            if serializer.is_valid(raise_exception=True):
                photo_inst = serializer.save()
                return Response({"success": f"Photo '{photo_inst.title_photo}' created successfully",
                                 "id": photo_inst.id})
        return Response({
            'status': 'failed'
        }, status=401)

    def put(self, request, pk=None, format=None):
        if pk:
            photo = self.get_object(pk, request)
            serializer = PhotoSerializer(photo, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response({"success": "Photo update", "update_photo": serializer.data})
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        raise MethodNotAllowed("put")

    def delete(self, request, pk=None, format=None):
        if pk:
            photo = self.get_object(pk, request)
            photo.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        raise MethodNotAllowed("delete")
