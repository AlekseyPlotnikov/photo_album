from django.urls import path, include
from .views import CreateUserView, TagAlbumModelView, TagPhotoModelView, PhotoAlbumModelView, PhotoModelView, \
    CurrentUserView
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView


app_name = "api"


urlpatterns = [
    path('auth/', include(('garpix_auth.urls', 'garpix_auth'), namespace='garpix_auth')),
    path('current-user/', CurrentUserView.as_view(), name='current-user'),
    path('create-user/', CreateUserView.as_view(), name='create-user'),
    path("photo-album-tag/", TagAlbumModelView.as_view(), name='photo-album-tags'),
    path("photo-tag/", TagPhotoModelView.as_view(), name='photo-tags'),
    path("photo-album/", PhotoAlbumModelView.as_view(), name='photo-album'),
    path("photo/", PhotoModelView.as_view(), name='photo'),
    path('photo-album/<int:pk>/', PhotoAlbumModelView.as_view(), name='photo-album-pk'),
    path('photo/<int:pk>/', PhotoModelView.as_view(), name='photo-pk'),

]
urlpatterns += [
    path('schema/', SpectacularAPIView.as_view(), name='schema'),
    path('swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    path('redoc/', SpectacularRedocView.as_view(url_name='schema'), name='redoc'),
]
